/*
	Exercice1
	Analyse
	Entree:
		nombreVentesVendeur

	Sortie:
		remunerationVendeur

	Donnees internes:
		SALAIRE_BASE_VENDEUR = 400
		COMMISSION_SUR_CHAQUE_VENTE = 200
		BONUS_SUR_TOTAL_VENTES = 0.05

	Traitement:
		Demander la saisie du nombre de ventes.
		revenuTotalSurVentes = nombreVentesVendeur * COMMISSION_SUR_CHAQUE_VENTE
		remunerationVendeur = SALAIRE_BASE_VENDEUR + revenuTotalSurVentes + revenuTotalSurVente * BONUS_SUR_TOTAL_VENTES
		remunerationVendeur = SALAIRE_BASE_VENDEUR + revenuTotalSurVentes * (1 + BONUS_SUR_TOTAL_VENTES)
*/

#include <iostream>

using namespace std;

int main()
{
	int nombreVentesVendeur;
	double remunerationVendeur, revenuTotalSurVentes;
	const double SALAIRE_BASE_VENDEUR = 400, COMMISSION_SUR_CHAQUE_VENTE = 200, BONUS_SUR_TOTAL_VENTES = 0.05;

	cout << "Saisir le nombre de ventes pour le vendeur: ";
	cin >> nombreVentesVendeur;

	while (cin.fail() || cin.peek() != '\n' || nombreVentesVendeur <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> nombreVentesVendeur;
	}

	revenuTotalSurVentes = nombreVentesVendeur * COMMISSION_SUR_CHAQUE_VENTE;

	remunerationVendeur = SALAIRE_BASE_VENDEUR + revenuTotalSurVentes * (1 + BONUS_SUR_TOTAL_VENTES);

	cout << "Le salaire du vendeur est " << remunerationVendeur << "$" << endl;

	system("pause");
	return 0;
}