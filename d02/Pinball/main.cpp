#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<alloc.h>
#include<dos.h>
#include<conio.h>
#include<graphics.h>
#define R 30
#define r 3
#define limite_g 100
#define limite_d 50
#define souris() _AX=0;\geninterrupt(0x33);\_AX=1;\geninterrupt(0x33);\_AX=8;\_CX=0;\_DX=479;\geninterrupt(0x33);\_AX=7;\_CX=0;\_DX=639;\geninterrupt(0x33);\
		 do\
		   {\
		    _AX=3;\geninterrupt(0x33);\
		    z1=_CX;\z2=_DX;\
		   }\
		   while(_BX!=1);\
		 if(z1>=253&&z1<=437&&z2>=143&&z2<=159)\vary=150;\_AX=2;\geninterrupt(0x33);\
		 if(z1>=252&&z1<=437&&z2>=163&&z2<=179)\vary=170;\_AX=2;\geninterrupt(0x33);\
		 if(z1>=253&&z1<=437&&z2>=183&&z2<=199)\vary=190;\_AX=2;\geninterrupt(0x33);\
		 if(z1>=253&&z1<=437&&z2>=203&&z2<=219)\vary=210;\_AX=2;\geninterrupt(0x33);\
		 if((z1<253||z1>437||z2<143||z2>221))\vary=0;\_AX=2;\geninterrupt(0x33);\

#define curseur() balle(x/2-5,vary,14);\
		  do\
		  {\
		   get=getch();\
		   if(get==72&&vary>130)\ {\vary-=20;\balle(x/2-5,vary,14);\balle(x/2-5,vary+20,4);\}\
		   if(get==72&&vary==130)\{\vary=210;\balle(x/2-5,vary,14);\balle(x/2-5,150,4);\balle(x/2-5,130,4);\}\
		   if((get==80)&&(vary<230))\ {\vary+=20;\balle(x/2-5,vary,14);\balle(x/2-5,vary-20,4);\}\
		   if((get==80)&&(vary==230))\{\vary=150;\balle(x/2-5,vary,14);\balle(x/2-5,210,4);\balle(x/2-5,230,4);\}\
		  }\
		  while(get!=13);\
		  balle(x/2-5,vary,4);\

int i, j, k, x, y, c, centrex, centrey, temoinx, temoiny, varx1, varx2, vary, point, life = 0, res;
int A[12][24];

main()
{
	void triangle(void);
	void barre(int, int, int, int, int);
	void board(void);
	void square(void);
	void balle_move(void);
	void rester1(void);
	void rester2(void);
	void menu(int, int);
	void entree(int);
	int t;
	int gdriver = DETECT, gmode, errorcode;
	initgraph(&gdriver, &gmode, "");
	errorcode = graphresult();
	if (errorcode != grOk)
	{
		printf("Graphics error: %s\n", grapherrormsg(errorcode));
		printf("Press any key to halt:");
		getch();
		exit(1);
	}
	x = getmaxx();
	y = getmaxy();
	entree(3000);
	menu(14, t);
	closegraph();
	return 0;
}
////////////////////////////////rectangle
void rect(void)
{
	void barre(int, int, int, int, int);
	barre(x / 2, 143, x / 2 + 104, 159, 1);
	barre(x / 2, 163, x / 2 + 104, 179, 1);
	barre(x / 2, 183, x / 2 + 104, 199, 1);
	barre(x / 2, 203, x / 2 + 104, 221, 1);
}
/*****************************************MENUPRINCIPAL*********************************************************/
void menu(int couleur, int t)
{
	void rect(void);
	void board(void);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	void curs(int, int);
	void men(int);
	int vary, chx, z1, z2;
	int get;
	vary = 150;
	board();
	barre(100, 50, x - 50, y - 50, 4);
	rect();
	men(couleur);
	//curseur();
	souris()
		curs(vary, t);
	getch();
}
/////////////fonction affichant les options se trouvant dans le menu principal
void men(int couleur)
{
	settextstyle(1, 0, 1);
	setcolor(10);
	outtextxy(x / 2, 120, "Menu principal");
	settextstyle(1, 0, 1);
	setcolor(couleur);
	outtextxy(x / 2, 140, "D�marrer");
	outtextxy(x / 2, 160, "Param�tres");
	outtextxy(x / 2, 180, "A ProPos");
	outtextxy(x / 2, 200, "Quitter");
}
////////////////////////////choix d'une option dans le menu principal
void curs(int choix, int t)
{
	void balle_move(int);
	void inst(void);
	void param(int);
	void men(int);
	void curs3(int);
	void menu(int, int);
	FILE*stoc;
	FILE*niveau;
	niveau = fopen("niveau", "r");
	fclose(niveau);
	fread(&t, sizeof(int), 1, stoc);
	fclose(stoc);
	if (choix == 150) balle_move(30);
	if (choix == 170) param(t);
	if (choix == 190) inst();
	if (choix == 0) menu(14, t);
	if (choix == 210) exit(1);
}
/******************************************PARAMETRES***********************************************************/
void param(int t)
{
	void par(int, int);
	void board(void);
	void level(void);
	void param(int);
	void rec(int);
	void curs2(int, int);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	int get, vary, z1, z2;
	vary = 150;
	board();
	barre(100, 50, x - 50, y - 50, 4);
	barre(x / 2, 143, x / 2 + 142, 159, 1);
	barre(x / 2, 163, x / 2 + 142, 179, 1);
	barre(x / 2, 183, x / 2 + 142, 199, 1);
	barre(x / 2, 203, x / 2 + 142, 221, 1);
	par(14, 10);
	souris()
		//curseur()
		curs2(vary, t);
	getch();
}
/////////////////fonction affichant les options se trouvant dans la fonction param�tres
void par(int couleur, int color)
{
	settextstyle(1, 0, 1);
	setcolor(color);
	outtextxy(x / 2, 120, "Param�tres");
	settextstyle(1, 0, 1);
	setcolor(couleur);
	outtextxy(x / 2, 140, "Niveau");
	outtextxy(x / 2, 160, "Multim�dia");
	outtextxy(x / 2, 180, "Meilleurs Scores");
	outtextxy(x / 2, 200, "Menu Principal");
}
///////////////////////fontion permettant de choisir une option dans "Param�tres"
void curs2(int chx, int t)
{
	void par(int, int);
	void level();
	void fds(void);
	void rec(int);
	void menu(int, int);
	if (chx == 150) level();
	if (chx == 170 || chx == 0) param(t);
	if (chx == 190) rec(t);
	if (chx == 210) menu(14, t);

}
//////////////////////Options de niveau
void niv(int coul, int col)
{
	settextstyle(1, 0, 1);
	setcolor(coul);
	outtextxy(x / 2, 120, "Niveau");
	settextstyle(1, 0, 1);
	setcolor(col);
	outtextxy(x / 2, 140, "Amateur");
	outtextxy(x / 2, 160, "Semi-Pro");
	outtextxy(x / 2, 180, "Pro");
	outtextxy(x / 2, 200, "Param�tres");
}
//////////////////////selection d'un niveau
void level(void)
{
	void niv(int, int);
	void board(void);
	void rect(void);
	void curs3(int);
	void menu(int, int);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	int get, vary, z1, z2, t;
	vary = 150;
	board();
	barre(100, 50, x - 50, y - 50, 4);
	rect();
	niv(10, 14);
	souris()
		//curseur()
		curs3(vary);
	getch();
}
//////////////////////choix d'un niveau
void curs3(int chx)
{
	void param(int);
	void niv(int, int);
	void menu(int, int);
	void level(void);
	int nivo;
	FILE*stoc;
	FILE*niveau;
	switch (chx)
	{
	case 150: nivo = 50; break;
	case 170: nivo = 30; break;
	case 190: nivo = 20; break;
	case 210: param(50); break;
	case 0: level(); break;
	}
	niveau = fopen("Niveau", "r");
	fclose(niveau);
	fwrite(&nivo, sizeof(int), 1, niveau);
	fclose(niveau);
	param(0);
	/*
	if(chx==150) menu(14,50);
	if(chx==170) menu(14,30);
	if(chx==190) menu(14,20);
	if(chx==210) param(50);
	if(chx==0) level();*/
}
/////////////////////affichage des meilleurs scores
void rec(int t)
{
	void barre(int, int, int, int, int);
	void board(void);
	void cling1(void);
	int perdu(int);
	int p, record, reco;
	char nom[21];
	FILE*score;
	board();
	barre(99, 50, 589, 429, 0);
	score = fopen("bestscore.txt", "r");
	fscanf(score, "%s", nom);
	fscanf(score, "%d", &record);
	gotoxy(40, 15);
	printf("%s", nom);
	gotoxy(42, 16);
	printf("%d", record);
	fclose(score);
	gotoxy(25, 20);
	printf("Presser entrer pour remettre � z�ro");
	cling1();
}
////////////////////////////////message clignotant
void cling1(void)
{
	void param(int);
	int d, t;
	FILE* vider;
	while (d != 27)
	{
		do
		{
			settextstyle(1, 0, 1);
			setcolor(14);
			outtextxy(150, y - 100, "Pressez esc pour revenir dans `Param�tres'!!!");
			delay(500);
			settextstyle(1, 0, 1);
			setcolor(1);
			outtextxy(150, y - 100, "Pressez esc pour revenir dans `Param�tres'!!!");
			delay(500);
		} while (kbhit() == 0);
		d = getch();
		if (d == 13)
		{
			vider = fopen("record2", "w+");
		}
	}
	param(t);
}
/*******************************************A_PROPOS************************************************************/
void inst(void)
{
	void board(void);
	void curs1(int);
	void men1(int, int);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	void ins(int);
	int vary, get, z1, z2;
	vary = 150;
	board();
	barre(99, 50, x - 50, y - 50, 4);
	barre(253, 143, 437, 159, 1);
	barre(253, 163, 437, 179, 1);
	barre(253, 183, 437, 199, 1);
	barre(253, 203, 437, 221, 1);
	men1(14, 10);
	souris()
		//curseur()
		curs1(vary);
	getch();
}
/////////////fonction affichant les options se trouvant dans la fonction Apropos
void men1(int couleur, int color)
{
	settextstyle(1, 0, 1);
	setcolor(color);
	outtextxy(253, 120, "A Propos");
	settextstyle(1, 0, 1);
	setcolor(couleur);
	outtextxy(253, 140, "Instructions");
	outtextxy(253, 160, "Historique");
	outtextxy(253, 180, "Facult� Des Sciences");
	outtextxy(253, 200, "Menu Principal");
}
///////////////////////////choix d'une option dans "Apropos"
void curs1(int chx)
{
	void menu(int, int);
	void men1(int, int);
	void ins(int);
	void hist(void);
	void fds(void);
	void inst(void);
	int t;
	if (chx == 150) ins(4);
	if (chx == 170) hist();
	if (chx == 210) menu(14, t);
	if (chx == 190) fds();
	if (chx == 0) inst();
}
///////////////////////////////message clignotant
void cling(void)
{
	void inst(void);
	int d;
	while (d != 27)
	{
		do
		{
			settextstyle(1, 0, 1);
			setcolor(14);
			outtextxy(150, y - 100, "Pressez esc pour revenir dans `A Propos'!!!");
			delay(500);
			settextstyle(1, 0, 1);
			setcolor(1);
			outtextxy(150, y - 100, "Pressez esc pour revenir dans `A Propos'!!!");
			delay(500);
		} while (kbhit() == 0);
		d = getch();
		if (d == 27) inst();
	}
}
////////////////////////////////////////les instructions
void ins1(int col)
{
	settextstyle(0, 0, 1);
	setcolor(col);
	outtextxy(130, 120, "Faites en sorte que la balle tombe sur la barre mobile,");
	outtextxy(130, 140, "en la d�pla�ant: avec la touche <--- vers la gauche et la");
	outtextxy(130, 160, "touche ---> vers la droite.");
	outtextxy(130, 180, "Si la balle touche le sol,vous perdrez des points de vie.");
}
//////////////////////////////////////////affichage des instructions
void ins(int couleur)
{
	void barre(int, int, int, int, int);
	void curs(int, int);
	void ins1(int);
	void cling(void);
	void board(void);
	int t;
	board();
	barre(99, 50, 589, 429, 3);
	ins1(couleur);
	cling();
	ins1(0);
	curs(190, t);
}
/////////////////////////////////////////historique du jeu
void hist1(int col)
{
	settextstyle(0, 0, 1);
	setcolor(col);
	outtextxy(130, 120, "Si vous ne le savez pas encore, ce jeu se trouve sur les");
	outtextxy(130, 140, "t�l�phones MOTOROLA et ALCATEL.");
	outtextxy(130, 160, "En ce qui concerne cette version, il s'agit d'un jeune ");
	outtextxy(130, 180, "�tudiant de la Facult� Des Sciences(fds) de l'UEH qui a ");
	outtextxy(130, 200, "eu conscience de sa faiblesse en programmation en MPC2 ");
	outtextxy(130, 220, "(cf.FDS).Pour combler ses lacunes, il a d�cid� de se ");
	outtextxy(130, 240, "donner commeprojet de r�aliser ce jeu qu'il a rebaptis�:");
	outtextxy(130, 260, "PING PONG. Et ce fameux �tudiant... il s'agit de moi,");
	outtextxy(130, 280, "Jeff ETIENNE. Je d�die ce jeu sp�cialement � mon cousin");
	outtextxy(130, 300, "Josu� ETIENNE, disparu lors du s�isme du 12 Janvier 2010");
	outtextxy(130, 320, "et je lui dis:");
	outtextxy(130, 340, "             `Rest in peace my Brother'");
}
////////////////////////////////affiche l'historique
void hist(void)
{
	void barre(int, int, int, int, int);
	void curs(int, int);
	void hist1(int);
	void cling(void);
	void board(void);
	int t;
	board();
	barre(99, 50, 589, 429, 3);
	hist1(4);
	cling();
	hist1(0);
	curs(190, t);
}
////////////////////////////////////////////////A propos de la Facult� des Sciences
void fds1(int col)
{
	settextstyle(0, 0, 1);
	setcolor(col);
	outtextxy(130, 120, "La Facult� Des Sciences(fds) comporte deux cycles");
	outtextxy(130, 140, "d'�tudes: Le premier est un cyle d'�tudes prop�-");
	outtextxy(130, 160, "deutiques dans lequel il y a la MPC1 et la MPC2");
	outtextxy(130, 180, "(MPC:Math�matiques Physique Chimie).Le second est");
	outtextxy(130, 200, "un cycle de sp�cialisation dans l'une des options");
	outtextxy(130, 220, "suivantes:G�nie Electronique,G�nie Electrom�canique");
	outtextxy(130, 240, "Architecture et G�nie civile.A la fin du premier cycle");
	outtextxy(130, 260, "d'�tudes(en MPC2),on vous soumet un projet d'informati-");
	outtextxy(130, 280, "que globalisant le cours de programmation en langage C.");
	outtextxy(130, 300, "Ce projet consiste en la r�alisation d'un jeu.");
	outtextxy(130, 320, "(GIPF,TOURNERIL,ECHEC...)");
}
//////////////////////////////////affiche les informations concernant la fds
void fds(void)
{
	void barre(int, int, int, int, int);
	void curs(int, int);
	void cling(void);
	void fds1(int);
	void board(void);
	int t;
	board();
	barre(99, 50, 589, 429, 3);
	fds1(4);
	cling();
	fds1(0);
	curs(190, t);
}
/****************************************PAGE_DENTREE**********************************************************/
void entree(int t)
{
	settextstyle(1, HORIZ_DIR, 1);
	outtextxy(x / 2 - 100, y / 2, "Les entreprises CEREBO");
	outtextxy(x / 2 - 50, y / 2 + 20, "pr�sentent...");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 100, y / 2, "Les entreprises CEREBO");
	outtextxy(x / 2 - 50, y / 2 + 20, "pr�sentent...");
	setcolor(15);
	outtextxy(x / 2 - 50, y / 2, "Jeffy production");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 50, y / 2, "Jeffy production");
	setcolor(15);
	outtextxy(x / 2 - 10, y / 2, "Dans...");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 10, y / 2, "Dans...");
	setcolor(15);
	settextstyle(4, HORIZ_DIR, 6);
	outtextxy(x / 2 - 150, y / 2 - 50, "PING PONG");
	delay(t);
	setcolor(0);
	settextstyle(4, HORIZ_DIR, 6);
	outtextxy(x / 2 - 150, y / 2 - 50, "PING PONG");
	setcolor(15);
	settextstyle(1, HORIZ_DIR, 1);
	outtextxy(x / 2 - 100, y / 2, "R�alis� par Jeff ETIENNE");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 100, y / 2, "R�alis� par Jeff ETIENNE");
	setcolor(15);
	outtextxy(x / 2 - 100, y / 2, "Avec la participation de");
	outtextxy(x / 2 - 65, y / 2 + 20, "Carlo production");
	delay(t);
	setcolor(0);
	settextstyle(1, HORIZ_DIR, 1);
	outtextxy(x / 2 - 100, y / 2, "Avec la participation de");
	outtextxy(x / 2 - 65, y / 2 + 20, "Carlo production");
	setcolor(15);
	outtextxy(x / 2 - 50, y / 2, "Special tanks to");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 50, y / 2, "Special tanks to");
	setcolor(15);
	outtextxy(x / 2 - 55, y / 2, "Carl-Hens FIEVRE");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 55, y / 2, "Carl-Hens FIEVRE");
	setcolor(15);
	outtextxy(x / 2 - 100, y / 2, "James-Feshner LECONTE");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 100, y / 2, "James-Feshner LECONTE");
	setcolor(15);
	outtextxy(x / 2 - 55, y / 2, "et Edgard ETIENNE");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 55, y / 2, "et Edgard ETIENNE");
	setcolor(15);
	outtextxy(x / 2 - 5, y / 2 - 80, "Ce");
	outtextxy(x / 2 - 45, y / 2 - 60, "programme");
	outtextxy(x / 2 - 100, y / 2 - 40, "est d�di� � un homme");
	outtextxy(x / 2 - 40, y / 2 - 20, "inoubliable");
	outtextxy(x / 2 - 55, y / 2, "Josu� ETIENNE");
	delay(t);
	setcolor(0);
	outtextxy(x / 2 - 5, y / 2 - 80, "Ce");
	outtextxy(x / 2 - 45, y / 2 - 60, "programme");
	outtextxy(x / 2 - 100, y / 2 - 40, "est d�di� � un homme");
	outtextxy(x / 2 - 40, y / 2 - 20, "inoubliable");
	outtextxy(x / 2 - 55, y / 2, "Josu� ETIENNE");
	do
	{
		setcolor(14);
		settextstyle(1, HORIZ_DIR, 1);
		outtextxy(x / 2 - 60, y / 2, "Pressez une touche!!!");
		delay(500);
		setcolor(0);
		settextstyle(1, HORIZ_DIR, 1);
		outtextxy(x / 2 - 60, y / 2, "Pressez une touche!!!");
		delay(500);
	} while (!kbhit());
	setcolor(10);
	settextstyle(1, HORIZ_DIR, 1);
	outtextxy(x / 2 - 10, y / 2, "loading...");
	delay(500);
	setcolor(0);
	settextstyle(1, HORIZ_DIR, 1);
	outtextxy(x / 2 - 10, y / 2, "loading...");
	getch();
}
/////////////////////////////////////donne le coup d'envoi
void balle_move(int t)
{
	void rebond1(int);
	void barre(int, int, int, int, int);
	void square(void);
	void triang(void);
	void board(void);
	void matrix(int, int);
	void balle(int, int, int);
	int d;
	point = 0;
	centrex = x / 2 + 24;
	centrey = y - 66;
	varx1 = x / 2;
	varx2 = x / 2 + 50;
	board();
	barre(10, 100, 80, 150, 0);
	barre(99, 50, 589, 429, 0);
	balle(x / 2 + 24, y - 66, 15);
	barre(varx1, y - 60, varx2, y - 55, 10);
	square();
	gotoxy(3, 8); printf("%d x vies", life);
	gotoxy(3, 9); printf("pts:%d", point);
	do
	{
		do
		{
			setcolor(14);
			settextstyle(1, HORIZ_DIR, 1);
			outtextxy(x / 2 - 100, y / 2, "Pressez entrer pour commencer!!!");
			delay(500);
			setcolor(0);
			settextstyle(1, HORIZ_DIR, 1);
			outtextxy(x / 2 - 100, y / 2, "Pressez entrer pour commencer!!!");
			delay(500);
		} while (!kbhit());
		d = getch();
		if (d == 13) rebond1(t); /*d�but du jeu*/
	} while (d != 13);

}
/****************************************QUITTER******************************************/
void quit(int posx1, int posy1, int posx2, int posx3, int resx, int resy, int t)
{
	void cursor1(int, int, int);
	void balle(int, int, int);
	int temx, temy;
	temx = resx; temy = resy;
	balle(posx1, posy1, 15);
	setfillstyle(1, 10);
	setcolor(10);
	bar(posx2, y - 60, posx3, y - 55);
	setcolor(14);
	settextstyle(1, 0, 1);
	outtextxy(x / 2 - 180, y / 2, "Voulez-vous retourner au menu principal?");
	outtextxy(x / 2 - 20, y / 2 + 40, "OUI   NON");
	cursor1(temx, temy, t);
}
/****************************************CURSOR1******************************************/
void cursor1(int temox, int temoy, int t)
{
	void rebond1(int);
	void rebond2(int);
	void rebond3(int);
	void rebond4(int);
	void menu(int, int);
	void balle(int, int, int);
	int touc, varx;
	varx = x / 2 - 30;
	balle(varx, y / 2 + 50, 1);
	do
	{
		touc = getch();
		if ((touc == 77) && (varx > x / 2 - 90)) { varx -= 60; balle(varx, y / 2 + 50, 1); balle(varx + 60, y / 2 + 50, 0); }
		if ((touc == 77) && (varx == x / 2 - 90)) { varx = x / 2 + 30; balle(varx, y / 2 + 50, 1); balle(x / 2 - 30, y / 2 + 50, 0); balle(x / 2 - 90, y / 2 + 50, 0); }
		if ((touc == 75) && (varx < x / 2 + 90)) { varx += 60; balle(varx, y / 2 + 50, 1); balle(varx - 60, y / 2 + 50, 1); }
		if ((touc == 75) && (varx == x / 2 + 90)) { varx = x / 2 - 30; balle(varx, y / 2 + 50, 1); balle(x / 2 + 30, y / 2 + 50, 0); balle(x / 2 + 90, y / 2 + 50, 0); }
		if ((touc == 13) && (varx == x / 2 - 30)) { balle(varx, y / 2 + 50, 0); life = 5; menu(14, t); }
		if (((touc == 13) && (varx == x / 2 + 30)) || touc == 27)
		{
			balle(varx, y / 2 + 50, 0);
			setcolor(0); settextstyle(1, 0, 1); outtextxy(x / 2 - 180, y / 2, "Voulez-vous retourner au menu principal?"); outtextxy(x / 2 - 20, y / 2 + 40, "OUI   NON");
			if (temox == 1 && temoy == 1) rebond1(t);
			if (temox == 2 && temoy == 2) rebond2(t);
			if (temox == 3 && temoy == 3) rebond3(t);
			if (temox == 4 && temoy == 4) rebond4(t);
		}
	} while (touc != 13);
}
/****************************************REBOND1******************************************/
void rebond1(int t)
{
	void horizontal_bottom1(int);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	void quit(int, int, int, int, int, int, int);
	void vert_d1(int);
	void triang(void);
	void rester(int);
	void rebond2(int);
	void rebond3(int);
	void rebond4(int);
	int c;
	x = getmaxx();
	y = getmaxy();
	do
	{
		do
		{
			delay(t);
			temoinx = 1; temoiny = 1;
			centrex = centrex + 5;
			centrey = centrey - 5;
			balle(centrex, centrey, 15);
			balle(centrex - 5, centrey + 5, 0);
			if ((centrey < 194) && (centrey > 83)) horizontal_bottom1(t);
			if ((centrex >= 117) && (centrey <= 193) && (centrey >= 68)) vert_d1(t);
			if ((centrex == x - 56) && (centrey > 53)) rebond2(t);
			if ((centrey == 53) && (centrex < x - 56)) rebond3(t);
			if ((centrey == 53) && (centrex == x - 56)) rebond4(t);
		} while (kbhit() == 0);
		c = getch();
		if (c == 77 && varx2 < 589) rester(1);
		if (c == 75 && varx1 > 99) rester(2);
		if (c == 77 && varx2 == 589) barre(539, y - 60, 589, y - 55, 10);
		if (c == 75 && varx1 == 99) barre(99, y - 60, 149, y - 55, 10);
		if (c == 27) quit(centrex, centrey, varx1, varx2, temoinx, temoiny, t);
	} while (c != 27);
}
/*********************************************REBOND2************************************************************/
void rebond2(int t)
{
	void horizontal_bottom2(int);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	void quit(int, int, int, int, int, int, int);
	void vert_g1(int);
	void rester(int);
	void rebond4(int);
	void rebond1(int);
	void rebond3(int);
	x = getmaxx();
	y = getmaxy();
	do
	{
		do
		{
			delay(t);
			temoiny = 2; temoinx = 2;
			centrex = centrex - 5;
			centrey = centrey - 5;
			balle(centrex, centrey, 15);
			balle(centrex + 5, centrey + 5, 0);
			if ((centrey <= 193) && (centrey >= 73)) horizontal_bottom2(t);
			if ((centrex <= x - 56) && (centrex >= 53) && (centrey <= 193) && (centrey >= 73)) vert_g1(t);
			if ((centrex == 103) && (centrey > 53)) rebond1(t);
			if ((centrex == 103) && (centrey == 53)) rebond3(t);
			if ((centrey == 53) && (centrex > 103) && (centrex < x - 56)) rebond4(t);
		} while (kbhit() == 0);
		c = getch();
		if (c == 77 && varx2 < 589) rester(1);
		if (c == 75 && varx1 > 99) rester(2);
		if (c == 77 && varx2 == 589) barre(539, y - 60, 589, y - 55, 10);
		if (c == 75 && varx1 == 99) barre(99, y - 60, 149, y - 55, 10);
		if (c == 27) quit(centrex, centrey, varx1, varx2, temoinx, temoiny, t);
	} while (c != 27);
}
/*****************************************************REBOND3**************************************************************/
void rebond3(int t)/*g�re le rebond de la balle sur le plafond et l'envoie a droite*/
{
	void horizontal_top1(int);
	void barre(int, int, int, int, int);
	void balle(int, int, int);
	void quit(int, int, int, int, int, int, int);
	void rester(int);
	void vert_d2(int);
	void rebond1(int);
	void rebond4(int);
	int perdu(int);
	x = getmaxx();
	y = getmaxy();
	do
	{
		do
		{
			delay(t);
			temoinx = 3; temoiny = 3;
			centrex = centrex + 5;
			centrey = centrey + 5;
			balle(centrex, centrey, 15);
			balle(centrex - 5, centrey - 5, 0);
			if ((centrey >= 67) && (centrey <= 193) && (centrex < x - 56))
			{
				horizontal_top1(t);
				vert_d2(t);
			}
			if ((centrex == x - 56) && (centrey < y - 66)) rebond4(t);
			if ((centrey == y - 66) && (centrex < x - 56) && (centrex >= varx1) && (centrex <= varx2)) rebond1(t);
			if ((centrey == y - 66) && (centrex == x - 56) && (centrex >= varx1) && (centrex <= varx2)) rebond2(t);
			perdu(t);
		} while (kbhit() == 0 && (centrey < y - 66));
		c = getch();
		if (c == 77 && varx2 < 589) rester(1);
		if (c == 75 && varx1 > 99) rester(2);
		if (c == 77 && varx2 == 589) barre(539, y - 60, 589, y - 55, 10);
		if (c == 75 && varx1 == 99) barre(99, y - 60, 149, y - 55, 10);
		if (c == 27) quit(centrex, centrey, varx1, varx2, temoinx, temoiny, t);
	} while (c != 27);
}
/******************************************REBOND4************************************************/
void rebond4(int t) /*g�re le rebond de la balle sur le mur droit et la fait monter*/
{
	void horizontal_top2(int);
	void balle(int, int, int);
	void barre(int, int, int, int, int);
	void quit(int, int, int, int, int, int, int);
	void rester(int);
	void rebond2(int);
	void rebond3(int);
	void vert_g2(int);
	int perdu(int);
	x = getmaxx();
	y = getmaxy();
	do
	{
		do
		{
			delay(t);
			temoiny = 4; temoinx = 4;
			centrex = centrex - 5;
			centrey = centrey + 5;
			balle(centrex, centrey, 15);
			balle(centrex + 5, centrey - 5, 0);
			if ((centrey > 67) && (centrey <= 193) && (centrex > 53))
			{
				horizontal_top2(t);
				vert_g2(t);
			}
			if ((centrey == y - 66) && (centrex > 103) && (centrex >= varx1) && (centrex <= varx2)) rebond2(t);
			if ((centrex == 103) && (centrey < y - 66)) rebond3(t);
			if ((centrex == 103) && (centrey == y - 66) && (centrex >= varx1) && (centrex <= varx2)) rebond1(t);
			perdu(t);
		} while (kbhit() == 0 && centrey < y - 66);
		c = getch();
		if (c == 77 && varx2 < 589) rester(1);
		if (c == 75 && varx1 > 99) rester(2);
		if (c == 77 && varx2 == 589) barre(539, y - 60, 589, y - 55, 10);
		if (c == 75 && varx1 == 99) barre(99, y - 60, 149, y - 55, 10);
		if (c == 27) quit(centrex, centrey, varx1, varx2, temoinx, temoiny, t);
	} while (c != 27);
}
//////////////////////////////Quand la balle touche le sol/////////////////////////
int perdu(int t)
{
	void menu(int, int);
	void rebond1(int);
	void barre(int, int, int, int, int);
	void balle(int, int, int);
	int record, p, q;
	char nomjoueur[21];
	FILE*score;
	if (centrey == y - 66 && (centrex <= varx1 || centrex >= varx2))
	{
		if (life > 0)
		{
			life--; gotoxy(3, 8); printf("%d", life);
			balle(centrex, centrey, 0);
			barre(varx1, y - 60, varx2, y - 55, 0);
			varx1 = x / 2; varx2 = x / 2 + 50;
			barre(varx1, y - 60, varx2, y - 55, 10);
			centrex = x / 2 + 24; centrey = y - 66;
			balle(centrex, centrey, 15);
			delay(1500);
			rebond1(t);
		}
		if (life == 0)
		{
			life = 5;
			while (kbhit() == 0)
			{
				setcolor(14);
				outtextxy(x / 2 - 40, y / 2 + 40, "Vous avez perdu !!!!!!!");
				delay(500);
				setcolor(1);
				outtextxy(x / 2 - 40, y / 2 + 40, "Vous avez perdu !!!!!!!");
				delay(500);
			}
			getch();
			setcolor(0);
			outtextxy(x / 2 - 40, y / 2 + 40, "Vous avez perdu !!!!!!!");
			score = fopen("bestscore.txt", "r+");
			fscanf(score, "%s", nomjoueur);
			fscanf(score, "%d", &record);
			if (point > record)
			{
				gotoxy(40, 16);
				printf("Ancien record: ");
				printf("%d", record);
				while (kbhit() == 0)
				{
					setcolor(14);
					outtextxy(x / 2 - 30, y / 2 + 30, "FELICITATIONS!!!");
					outtextxy(x / 2 - 90, y / 2 + 50, "Vous avez battu un record");
				}
				getch();
				setcolor(0);
				outtextxy(x / 2 - 30, y / 2 + 30, "FELICITATIONS!!!");
				outtextxy(x / 2 - 90, y / 2 + 50, "Vous avez battu un record");
				record = point;
				setcolor(14);
				outtextxy(x / 2 - 20, y / 2 + 30, "Entrer votre nom");
				getch();
				setcolor(0);
				outtextxy(x / 2 - 20, y / 2 + 30, "Entrer votre nom");
				gotoxy(40, 18);
				printf("Nouveau record:%d", record);
				gotoxy(40, 20); scanf("%s", nomjoueur);
				fprintf(score, "%s : ", nomjoueur);
				fprintf(score, "%d", record);
				fclose(score);
			}
			menu(14, t);
		}
	}
	return record;
}
/////////////////////dessine l'environnement du jeu///////////////////////////
void board(void)
{
	void barre(int, int, int, int, int);
	x = getmaxx();
	y = getmaxy();
	barre(0, 0, x, limite_d, 12);
	barre(x - 50, 50, x, y - 50, 12);
	barre(0, limite_d, limite_g, y, 12);
	for (i = 0; i < 32; i++)
	{
		for (j = 0; j < 5; j++)
		{
			setcolor(3);
			line(i * 20, j * 10, i * 20, 5 + j * 10);
			line(10 + i * 20, 5 + j * 10, 10 + i * 20, 10 + j * 10);
			line(0, j * 5, x, j * 5);
			line(0, 25 + j * 5, x, 25 + j * 5);
		}
	}
	for (i = 1; i < 6; i++)
	{
		for (j = 0; j < 76; j++)
		{
			setcolor(3);
			line(0, limite_d + j * 5, limite_g, 50 + j * 5);
			line(i * 20, 50 + j * 10, i * 20, 55 + j * 10);
			line(10 + (i - 1) * 20, 55 + j * 10, 10 + (i - 1) * 20, 60 + j * 10);
		}
	}
	for (i = 1; i < 3; i++)
	{
		for (j = 0; j < 77; j++)
		{
			line(x - 40 + (i - 1) * 20, 50 + j * 10, x - 40 + (i - 1) * 20, 55 + j * 10);
			line(x - 30 + (i - 1) * 20, 55 + j * 10, x - 30 + (i - 1) * 20, 60 + j * 10);
			line(x - 50, 50 + j * 5, x, 50 + j * 5);
		}
	}
	setcolor(12);
	bar(0, y - 50, x, y);
	setcolor(3);
	line(0, y - 50, 100, y - 50);
	line(x - 50, y - 50, x, y - 50);
	for (i = 1; i < 33; i++)
	{
		for (j = 0; j < 10; j++)
		{
			setcolor(3);
			line((i - 1) * 20, y - 50 + j * 10, (i - 1) * 20, y - 45 + j * 10);
			line(10 + (i - 1) * 20, y - 45 + j * 10, 10 + (i - 1) * 20, y - 40 + j * 10);
			line(0, y - 45 + j * 5, x, y - 45 + j * 5);
		}
	}
	barre(99, 50, 100, y - 50, 4);
	setcolor(0);
	rectangle(0, 0, x, y);
	rectangle(limite_g, limite_d, x - 50, y - 50);
}
////////////////////////////dessine une barre////////////////////////////
void barre(int x1, int y1, int x2, int y2, int col)
{
	setfillstyle(1, col);
	bar(x1, y1, x2, y2);
}
/////////////////////////////dessine une balle///////////////////////////
void balle(int Ox, int Oy, int col)
{
	setfillstyle(1, col);
	setcolor(col);
	pieslice(Ox, Oy, 0, 360, r);
}
///////////////////////////g�re la barre mobile//////////////////////////
void rester(int a)
{
	void barre(int, int, int, int, int);
	x = getmaxx();
	y = getmaxy();
	if (a == 1)
	{
		varx1 = varx1 + 20;
		varx2 = varx2 + 20;
		barre(varx1, y - 60, varx2, y - 55, 10);
		barre(varx1 - 20, y - 60, varx1, y - 55, 0);
	}
	if (a == 2)
	{
		varx1 = varx1 - 20;
		varx2 = varx2 - 20;
		barre(varx1, y - 60, varx2, y - 55, 10);
		barre(varx2, y - 60, varx2 + 20, y - 55, 0);
	}
}
/******************************************VERT_D1***************************/
void vert_d1(int t)
{
	void rebond2(int);
	void rebond1(int);
	int l;
	for (j = 1; j < 24; j++)
	{
		if (centrex <= 103 + j * 20 && centrex >= 98 + j * 20)
		{
			i = (centrey - 70) / 10;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond2(t);
			}
			if (A[i][j] == 0) rebond1(t);
		}
	}
}
/******************************************VERT_D2***************************/
void vert_d2(int t)
{
	void rebond3(int);
	void rebond4(int);
	for (j = 1; j < 24; j++)
	{
		if (centrex <= 103 + j * 20 && centrex >= 98 + j * 20)
		{
			i = (centrey - 70) / 10;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond4(t);
			}
			if (A[i][j] == 0) rebond3(t);
		}
	}
}
/******************************************VERT_G2***************************/
void vert_g2(int t)
{
	void rebond3(int);
	void rebond4(int);
	int l;
	for (j = 0; j < 24; j++)
	{
		if (centrex <= 123 + j * 20 && centrex >= 118 + j * 20)
		{
			i = (centrey - 70) / 10;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond3(t);
			}
			if (A[i][j] == 0) rebond4(t);
		}
	}
}
/******************************************VERT_G1***************************/
void vert_g1(int t)
{
	void rebond2(int);
	void rebond1(int);
	for (j = 0; j < 23; j++)
	{
		if (centrex <= 123 + j * 20 && centrex >= 118 + j * 20)
		{
			i = (centrey - 70) / 10;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond1(t);
			}
			if (A[i][j] == 0) rebond2(t);
		}
	}
}
/******************************************HORIZONTAL_BOTTOM1*************************************************/
void horizontal_bottom1(int t)
{
	void rebond3(int);
	void rebond4(int);
	void rebond1(int);
	for (i = 0; i < 12; i++)
	{
		if (centrey == 83 + i * 10 && centrex >= 103 && centrex < x - 56)
		{
			j = (centrex - 100) / 20;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond3(t);
			}
			if (A[i][j] == 0) rebond1(t);
		}
		if (centrey == 83 + i * 10 && centrex == x - 56)
		{
			j = 23;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond4(t);
			}
			if (A[i][j] == 0) rebond2(t);
		}
	}
}
/******************************************HORIZONTAL_BOTTOM2*************************************************/
void horizontal_bottom2(int t)
{
	void rebond4(int);
	void rebond2(int);
	void rebond3(int);
	for (i = 0; i < 12; i++)
	{
		if (centrey == 83 + i * 10 && centrex > 103 && centrex <= x - 56)
		{
			j = (centrex - 100) / 20;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond4(t);
			}
			if (A[i][j] == 0) rebond2(t);
		}
		if (centrey == 83 + i * 10 && centrex == 103)
		{
			j = 0;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond3(t);
			}
			if (A[i][j] == 0) rebond1(t);
		}
	}
}
/******************************************HORIZONTAL_TOP1****************************************************/
void horizontal_top1(int t)
{
	void rebond3(int);
	void rebond1(int);
	void rebond2(int);
	for (i = 0; i < 12; i++)
	{
		if (centrey == 68 + i * 10 && centrex >= 103 && centrex < x - 56)
		{
			j = (centrex - 100) / 20;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond1(t);
			}
			if (A[i][j] == 0)
				rebond3(t);
		}
		if (centrey == 68 + i * 10 && centrex == x - 56)
		{
			j = 23;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond2(t);
			}
			if (A[i][j] == 0)
				rebond4(t);
		}
	}
}
/******************************************HORIZONTAL_TOP2****************************************************/
void horizontal_top2(int t)
{
	void rebond4(int);
	void rebond2(int);
	void rebond1(int);
	void rebond3(int);
	for (i = 0; i < 12; i++)
	{
		if (centrey == 68 + i * 10 && centrex > 103 && centrex <= x - 56)
		{
			j = (centrex - 100) / 20;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond2(t);
			}
			if (A[i][j] == 0)
				rebond4(t);
		}
		if (centrey == 68 + i * 10 && centrex == 103)
		{
			j = 0;
			if (A[i][j] == 1)
			{
				A[i][j] = 0;
				setfillstyle(1, 0);
				bar(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				point++;
				gotoxy(7, 9); printf("%d", point);
				rebond1(t);
			}
			if (A[i][j] == 0)
				rebond3(t);
		}
	}
}
/******************************************MATRICE*************************************/
void matrice(void)
{
	int m, n;
	x = getmaxx();
	y = getmaxy();
	m = 12; n = 24;
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			if (A[i][j] == 1)
			{
				setfillstyle(1, 4 + i);
				bar(j * 20 + 100, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				setcolor(3);
				rectangle(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
			}
			if (A[i][j] == 0)
			{
				setfillstyle(1, 0);
				bar(j * 20 + 100, 70 + i * 10, 120 + j * 20, 80 + i * 10);
				setcolor(0);
				rectangle(100 + j * 20, 70 + i * 10, 120 + j * 20, 80 + i * 10);
			}
		}
	}
}
/**************************************************square************************************************/
void square(void)
{
	void matrice(void);
	void triang(void);
	int m, n;
	x = getmaxx();
	y = getmaxy();
	n = 24; m = 12;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
			A[j][i] = 1;
	}

	matrice();
}
/*****************************************************TRIANGLE**********************************************************/
void triang(void)
{
	void matrice(void);
	int m, n;
	n = 24; m = 12;
	x = getmaxx();
	y = getmaxy();
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
			A[i][j] = 1;
	}
	for (i = 1; i < m; i++)
	{
		for (j = 0; j < i; j++)
			A[i][j] = 0;
	}
	for (i = 1; i < m; i++)
	{
		for (j = n - 1; j > n - 1 - i; j--)
			A[i][j] = 0;
	}
	matrice();
}
