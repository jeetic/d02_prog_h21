/*
	Exercice 4
	Analyse
	Entree:
		facture

	Sortie:
		montantPourboire

	Donnees internes:
		POURBOIRE = 0.15
		SEUIL_POURBOIRE = 1

	Traitement:
		Si facture est plus grand que SEUIL_POURBOIRE on calcul le montant du pourboire
		montantPourboire = POURBOIRE * facture
*/

#include<iostream>

using namespace std;

int main()
{
	double facture, montantPourboire;
	const double POURBOIRE = 0.15, SEUIL_POURBOIRE = 1;

	cout << "Saisir le montant de la facture : ";
	cin >> facture;

	while (cin.fail() || cin.peek() != '\n' || facture <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> facture;
	}

	if (facture < SEUIL_POURBOIRE)
	{
		cout << "Pas de pourboire pour cette facture" << endl;
	}
	else
	{
		montantPourboire = POURBOIRE * facture;
		cout << "Le montant du pourboire est " << montantPourboire << "$" << endl;
	}

	system("pause");
	return 0;
}