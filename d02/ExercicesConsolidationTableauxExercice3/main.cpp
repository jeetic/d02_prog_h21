#include <iostream>

using namespace std;

/*
a) �crire une fonction qui initialise chaque case d'un tableau d'entiers avec le cube de son indice.

	Analyse
	Entree
		--
	Sortie
		tableau initialise

	Donnees internes
		--

	Traitement
		Pour toute valeur entiere inferieure a la taille du tableau, 
		l'element a l'indice egale a cette valeur est le cube de cette valeur.

*/
void initCube(int tab[], int nbElts);

/*
	b.�crire une fonction qui retourne la moyenne d'un tableau de float pass� en param�tre.
	Analyse
	Entree
		tableau de float
	Sortie
		moyenne des valeurs du tableau

	Donnees internes
		--

	Traitement
		diviser la somme des elements du tableau par la taille du tableau.
*/
float moyenne(float tab[], int nbElts);

/*
	c.�crire une fonction qui re�oit un tableau d'entiers et un entier n. 
	Elle retourne true si n est dans le tableau, false sinon.
	Analyse
	Entree
		tableau d'entiers
		entier n
	Sortie
		Si n est un element du tableau retourne true, sinon false

	Donnees internes
		--

	Traitement
		Parcourir les elements du tableau. Si n est un element du tableau retourne true, sinon false
*/
bool contient(int tab[], int nbElts, int n);

/*
	d. �crire une fonction qui re�oit un tableau d'entiers et un entier n. 
	Cette fonction doit retourner le nombre de fois (=nombre d'occurrences) que cet entier n se trouve dans le tableau.
	Analyse
	Entree
		tableau d'entiers
		entier n
	Sortie
		nombre de fois que n'est est repete dans le tableau

	Donnees internes
		--

	Traitement
		Parcourir les elements du tableau. Si n est un element du tableau ajouter un au nombre de fois
*/
int occurrences(int tab[], int nbElts, int n);

/*
	e) �crire une fonction qui re�oit un tableau de float et affiche les indices des valeurs strictement n�gatives ainsi que les valeurs en question.
	Analyse
	Entree
		tableau de float
	Sortie
		indices des valeurs negatives
		les valeurs negatives

	Donnees internes
		--

	Traitement
		Parcourir les elements du tableau. Si une valeur est negative, l'afficher ainsi que son indice.
*/

void afficherNegatifs(float tab[], int nbElts);

/*
	f) �crire une fonction qui re�oit un tableau d'entiers et un entier n. 
	Cette fonction retourne le nombre d'�l�ments du tableau qui sont plus grands ou �gaux � n.
	Analyse
	Entree
		tableau d'entiers
		entier n
	Sortie
		nombre d'elements du tableau superieurs ou egaux a n.

	Donnees internes
		--

	Traitement
		Parcourir les elements du tableau. Si n est inferieur ou egal a un element du tableau ajouter un au nombre de fois
*/

int plusGrandsElements(int tab[], int nbElts, int n);

/*
	g. �crire une fonction qui re�oit un tableau d'entiers et un entier n. 
	La fonction effectue n rotations � droite des �l�ments contenus dans les cases du tableau. 
	� chaque rotation, le dernier �l�ment du tableau se place dans la premi�re case.
	Par exemple, si le tableau re�u contient : 4 3 8 3 6 4 9 5 0 1 5 2 
	et que n vaut 4 alors apr�s l'appel, le tableau contiendra : 0 1 5 2 4 3 8 3 6 4 9 5

	Analyse
	Entree
		tableau d'entiers
		entier n
	Sortie
		Afficher tableau apres n rotations

	Donnees internes
		--

	Traitement
		A chaque rotation, Ajouter un sur l'indice de chaque element du tableau et mettre le dernier element a la premiere indice.
*/
void rotationDroite(int tab[], int nbElts, int n);

int main()
{
	const int TAILLE = 12;

	int tab[TAILLE] = { 4, 3, 8, 3, 6, 4, 9, 5, 0, 1, 5, 2 };

	rotationDroite(tab, TAILLE, 11);

	system("pause");

	return 0;
}

void initCube(int tab[], int nbElts)
{
	for (int i = 0; i < nbElts; i++)
	{
		tab[i] = i * i * i;
	}
}

float moyenne(float tab[], int nbElts)
{
	float somme = 0;

	for (int i = 0; i < nbElts; i++)
	{
		somme += tab[i];
	}

	return somme / nbElts;
}

bool contient(int tab[], int nbElts, int n)
{
	bool existe = false;
	for (int i = 0; i < nbElts; i++)
	{
		if (tab[i] == n)
		{
			existe = true;
		}
	}
	return existe;
}

int occurrences(int tab[], int nbElts, int n)
{
	int nombreFois = 0;
	for (int i = 0; i < nbElts; i++)
	{
		if (tab[i] == n)
		{
			nombreFois++;
		}
	}

	return nombreFois;
}

void afficherNegatifs(float tab[], int nbElts)
{
	for (int i = 0; i < nbElts; i++)
	{
		if (tab[i] < 0)
		{
			cout << "Le nombre a la position " << i << " est negatif" << endl;
			cout << "Ce nombre est " << tab[i] << endl;
		}
	}
}

int plusGrandsElements(int tab[], int nbElts, int n)
{
	int nbElementSuperieurAn = 0;

	for (int i = 0; i < nbElts; i++)
	{
		if (tab[i] >= n)
		{
			nbElementSuperieurAn++;
		}
	}
	return nbElementSuperieurAn;
}

void rotationDroite(int tab[], int nbElts, int n)
{
	int temp;
	int temp1;
	int temp2;
	
	for (int j = 0; j < n; j++)
	{
		temp1 = tab[1];
		tab[1] = tab[0];
		temp = tab[nbElts - 1];
		for (int i = 1; i < nbElts - 1; i++)
		{
			if (i % 2 == 0)
			{
				temp1 = tab[i + 1];
				tab[i + 1] = temp2;
			}
			else
			{
				temp2 = tab[i + 1];
				tab[i + 1] = temp1;
			}
		}
		tab[0] = temp;
	}
	

	for (int i = 0; i < nbElts; i++)
	{
		cout << tab[i] << " ";
	}
	cout << endl;
}