#include <iostream>

using namespace std;

/*
	Analyse
	Entree
		valeur de type long long

	Sortie
		valeur de type long long validee

	Donnees internes
		--

	Traitement
		Demander la saisie de la valeur tant que l'utilisateur ne saisit pas une valeur de type long long.
*/

long long validerLongLong(long long valeurSaisie);

long long validerLongLongPlusGrandQue(long long valeurSaisie, long long valeurSeuil);

int main()
{

	system("pause");

	return 0;
}

long long validerLongLong(long long valeurSaisie)
{
	cout << "Saisir un nombre de type long long : ";
	cin >> valeurSaisie;

	while (cin.fail() || cin.peek() != '\n')
	{
		cin.clear();
		cin.ignore(512, '\n');

		cout << "Attention!!! Vous devez saisir un nombre de type long long : ";
		cin >> valeurSaisie;
	}

	return valeurSaisie;
}

long long validerLongLongPlusGrandQue(long long valeurSaisie, long long valeurSeuil)
{
	do
	{
		valeurSaisie = validerLongLong(valeurSaisie);

		if (valeurSaisie <= valeurSeuil)
		{
			cout << "Attention!!! Vous devez saisir une valeur plus grande que " << valeurSeuil << endl;
		}
		
	} while (valeurSaisie <= valeurSeuil);

	return valeurSaisie;
}
