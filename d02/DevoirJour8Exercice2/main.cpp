#include <iostream>

using namespace std;

void uneFonction(int a, int b, int& c, int* d, int e);

int main()
{
	int val1 = 4, val2 = 2, reponse1, reponse2, reponse3 = 0;

	uneFonction(val1, val2, reponse1, &reponse2, reponse3);
	cout << reponse1 << endl;
	cout << reponse2 << endl;
	cout << reponse3 << endl;

	system("pause");

	return 0;
}

void uneFonction(int a, int b, int& c, int* d, int e)
{
	c = a + b;
	*d = a * b;
	e = a - b;
}

/*
	a) Par copie

	b) par copie

	c) par reference

	d) par adresse

	e) par copie

	f) La valeur 6 sera affichee. Car dans la fonction uneFonction, le 3eme parametre est passe par reference a la fonction. 
		Cette valeur est 2 + 4 (ligne 16). Donc reponse1 sera modifiee apres l'appel de la fonction

	g) La valeur 8 sera affichee. le 4eme parametre est passe par adresse a la fonction. Sa valeur est a * b (ligne 17).
		Donc reponse2 sera modifiee apres l'appel de la fonction

	h) La valeur 0 sera affichee. Car le dernier parametre est passe par copie a la fonction. Donc reponse3 ne sera pas modifiee
		apres l'appel de la fonction.

	e) 
*/