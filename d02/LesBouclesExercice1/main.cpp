/*
	Exercice 1
	�crire un programme qui lit un nombre tant que la valeur contenu dans celle-ci n�est pas entre 0 et 100 inclusivement.

	Analyse
	Entree
		Valeur saisie par l'utilisateur (valeurSaisie)

	Sortie
		--

	Donnees internes
		borne inferieure: 0 (MIN)
		borne superieure: 100 (MAX)

	Traitement
		Lire le nombre tant qu'il n'est pas compris entre MIN et MAX
*/

#include<iostream>

using namespace std;

int main() {
	double	valeurSaisie;

	const double MIN = 0;
	const double MAX = 100;

	cout << "Saisir un nombre entre " << MIN << " et " << MAX << endl;
	cin >> valeurSaisie;
	while (cin.fail() || cin.peek() != '\n' || !(valeurSaisie >= MIN && valeurSaisie <= MAX)) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre superieur ou egal a " << MIN << " et inferieur ou egal a " << MAX << ": ";
		cin >> valeurSaisie;
	}

	system("pause");

	return 0;
}