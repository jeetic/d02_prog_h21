#include <iostream>

using namespace std;

struct etudiant
{
	char nom[41];
	char prenom[76];
	unsigned int codeEtudiant;
	unsigned short age;
};

int main()
{
	const locale LOCALE = locale::global(locale(""));
	etudiant etudiant;

	cout << "Nom de l'�tudiant : ";
	cin >> etudiant.nom;

	cout << "Pr�nom de l'�tudiant : ";
	cin >> etudiant.prenom;

	cout << "Code de l'�tudiant : ";
	cin >> etudiant.codeEtudiant;

	cout << "Age de l'�tudiant : ";
	cin >> etudiant.age;

	cout << "---------------- Affichage ----------------------" << endl;
	cout << "Nom de l'�tudiant    : " << etudiant.nom << endl;
	cout << "Pr�nom de l'�tudiant : " << etudiant.prenom << endl;
	cout << "Code de l'�tudiant   : " << etudiant.codeEtudiant << endl;
	cout << "Age de l'�tudiant    : " << etudiant.age << endl;

	system("pause");

	return 0;
}