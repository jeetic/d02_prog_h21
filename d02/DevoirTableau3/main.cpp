#include <iostream>
using namespace std;

int validerSaisie(int valeurSaisie);

int main()
{
	//i)
	int tabEntiers[12] = { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34 };
	//ii)
	char direction[4] = { 'N', 'S', 'E', 'O' };
	//iii)
	const float VALEURS_CONSTANTES[6] = { 0.005, -0.032, 0.000001, 0.167, -100000.3, 0.015 };
	//iv)
	int tabSaisiUser[10];

	//i)
	for (int i = 0; i < 12; i++)
	{
		cout << "Element " << i << " : " << tabEntiers[i] << endl;
	}

	//ii)
	for (int i = 0; i < 4; i++)
	{
		cout << "Element " << i << " : " << direction[i] << endl;
	}

	//iii)
	for (int i = 0; i < 6; i++)
	{
		cout << "Element " << i << " : " << VALEURS_CONSTANTES[i] << endl;
	}

	//i)
	for (int i = 0; i < 10; i++)
	{
		cout << "Saisi le " << i + 1 << "e nombre entier : " << endl;
		cin >> tabSaisiUser[i];
		tabSaisiUser[i] = validerSaisie(tabSaisiUser[i]);
	}

	for (int i = 0; i < 10; i++)
	{
		cout << "Element " << i << " : " << tabSaisiUser[i] << endl;
	}

	system("pause");

	return 0;
}

int validerSaisie(int valeurSaisie)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> valeurSaisie;
	}
	return valeurSaisie;
}