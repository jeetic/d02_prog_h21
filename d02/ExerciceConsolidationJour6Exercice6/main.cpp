/*
	Exercice 6
	Analyse
	Entree
		nombreDeJours
		nombreKilowattheures

	Sortie
		montantFacture

	Donnees internes
		TARIF_JOURNALIER = 0.50
		TARIF_KILOWATTHEURE = 0.30
		SEUIL_REDUCTION = 200
		TARIF_REDUIT = 0.20

	Traitement
		Si la consomation est moins de SEUIL_REDUCTION, additionner les produits de nombreDeJours par TARIF_JOURNALIER et nombreKilowattheures par TARIF_KILOWATTHEURE
		Sinon additionner les produits de nombreDeJours par TARIF_JOURNALIER et nombreKilowattheures par TARIF_REDUIT
*/

#include<iostream>

using namespace std;

int main() {
	int nombreDeJours;
	double nombreKilowattheures, montantFacture;

	const double TARIF_JOURNALIER = 0.50, TARIF_KILOWATTHEURE = 0.30, SEUIL_REDUCTION = 200, TARIF_REDUIT = 0.20;

	cout << "Saisir le nombre de jours : ";
	cin >> nombreDeJours;

	while (cin.fail() || cin.peek() != '\n' || nombreDeJours <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> nombreDeJours;
	}
	
	cout << "Saisir le nombre de kilowattheures : ";
	cin >> nombreKilowattheures;

	while (cin.fail() || cin.peek() != '\n' || nombreKilowattheures <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> nombreKilowattheures;
	}
	
	if (nombreKilowattheures <= SEUIL_REDUCTION) {
		montantFacture = nombreDeJours * TARIF_JOURNALIER + nombreKilowattheures * TARIF_KILOWATTHEURE;
	}
	else
	{
		montantFacture = nombreDeJours * TARIF_JOURNALIER + nombreKilowattheures * TARIF_REDUIT;
	}

	cout << "Le montant total de la facture est " << montantFacture << "$" << endl;

	system("pause");

	return 0;
}