/*
	Exercice 5
	Analyse
	Entree
		--

	Sortie
		Afficher les nombres premiers entre 1 et 50000

	Donnees internes
		BORNE_INFERIEURE = 1
		BORNE_SUPERIEURE = 50000

	Traitement
		Verifier si chacun des nombres entre 1 et 50000 est premier et l'afficher si c'est le cas
*/

#include<iostream>

using namespace std;

int main() {
	int nombreAvant, reste;
	const int BORNE_INFERIEURE = 1, BORNE_SUPERIEURE = 50000;

	cout << "Les nombres premiers entre " << BORNE_INFERIEURE << " et " << BORNE_SUPERIEURE << " sont" << endl;
	for (int I = BORNE_INFERIEURE; I <= BORNE_SUPERIEURE; I++)
	{
		if (I < 4)
			cout << I << endl;
		else
		{
			nombreAvant = 2;
			do
			{
				reste = I % nombreAvant;
				nombreAvant = nombreAvant + 1;
			} while (reste != 0 && nombreAvant < I);

			if (reste != 0) {
				cout << I << endl;
			}
		}
	}

	system("pause");

	return 0;
}