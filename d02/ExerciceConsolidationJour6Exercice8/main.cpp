/*
	Exercice 8
	Analyse
	Entree
		resultat piece 1
		resultat piece 2

	Sortie
		Afficher si la personne a gagn�
		Afficher le montant du gain

	Donnees internes
		MONTANT_PIECES_FACE = 10
		MONTANT_PIECE1_FACE_PIECE2_PILE = 5
		POSIBILITE1_PIECE = 'F'
		POSIBILITE2_PIECE = 'P'

	Traitement
		Si piece 1 donne face, la personne a gagn�.
		Si piece 1 et piece 2 donnent face alors Montant du gain = MONTANT_PIECES_FACE
		Si piece 1 donne face et piece 2 donne pile alors Montant du gain = MONTANT_PIECE1_FACE_PIECE2_PILE
*/

#include<iostream>

using namespace std;

int main()
{
	char resultatPiece1, resultatPiece2;

	const double MONTANT_PIECES_FACE = 10, MONTANT_PIECE1_FACE_PIECE2_PILE = 5, POSIBILITE1_PIECE = 'F', POSIBILITE2_PIECE = 'P';

	cout << "Entrer le resultat de la premiere piece. Vous devez entrer " << POSIBILITE1_PIECE << " ou " << POSIBILITE2_PIECE << ": ";
	cin >> resultatPiece1;
	while (cin.fail() || cin.peek() != '\n' || resultatPiece1 != POSIBILITE1_PIECE || resultatPiece1 != POSIBILITE2_PIECE)
	{
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Vous devez entrer " << POSIBILITE1_PIECE << " ou " << POSIBILITE2_PIECE << ": ";
		cin >> resultatPiece1;
	}

	cout << "Entrer le resultat de la deuxieme piece. Vous devez entrer " << POSIBILITE1_PIECE << " ou " << POSIBILITE2_PIECE << ": ";
	cin >> resultatPiece2;
	while (cin.fail() || cin.peek() != '\n' || resultatPiece2 != POSIBILITE1_PIECE || resultatPiece2 != POSIBILITE2_PIECE)
	{
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Vous devez entrer " << POSIBILITE1_PIECE << " ou " << POSIBILITE2_PIECE << ": ";
		cin >> resultatPiece2;
	}

	if (resultatPiece1 == POSIBILITE1_PIECE && resultatPiece2 == POSIBILITE1_PIECE)
	{
		cout << "Le montant du gain est " << MONTANT_PIECES_FACE << "$" << endl;
	}
	else if (resultatPiece1 == POSIBILITE1_PIECE && resultatPiece2 == POSIBILITE2_PIECE)
	{
		cout << "Le montant du gain est " << MONTANT_PIECE1_FACE_PIECE2_PILE << "$" << endl;
	}
	else
	{
		cout << "Vous avez perdu" << endl;
	}

	system("pause");

	return 0;
}