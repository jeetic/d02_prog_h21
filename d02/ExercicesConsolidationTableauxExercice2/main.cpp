/*
	Analyse
	Entree
		tableau de 20 entiers x
		tableau de 20 entiers Y

	Sortie
		X
		Y
		tableau des produits des elements de X et Y nomme Z
		Racine carre de la somme des elements de Z

	Donnees internes
		TAILLE_TABLEAU = 20
		NOMBRE_ENTIERS = 40

	Traitement
		Demander a l'utilisateur de saisir NOMBRES_ENTIERS entiers.
		Placer les TAILLE_TABLEAU premiers dans X et le reste dans Y.
		Parcourir les tableau X et Y et mettre le produit des elements de meme indice dans Z.
		Afficher les elements de X, Y et Z dans une table.
		Afficher la racine carre de la somme des elements de Z.

*/

#include <iostream>
#include <math.h>

using namespace std;

void validerNumerique(int* nombreSaisi);
void calculTailleNombre(int nombre, int &taille);
void afficherNombre(int nombre);

int main()
{
	const int TAILLE_TABLEAU = 20, NOMBRE_ENTIERS = 40;

	int X[TAILLE_TABLEAU], Y[TAILLE_TABLEAU], Z[TAILLE_TABLEAU];
	int nombreSaisi;
	int sommeZ = 0;
	double racineCarree;


	char texte[10] = "";

	/*transformerNombreEnTexte(255, texte);
	cout << texte << endl;*/

	for (int i = 0; i < TAILLE_TABLEAU; i++)
	{
		cout << "Saisir un entier : ";
		cin >> nombreSaisi;

		validerNumerique(&nombreSaisi);
		X[i] = nombreSaisi;
	}

	for (int i = 0; i < TAILLE_TABLEAU; i++)
	{
		cout << "Saisir un entier : ";
		cin >> nombreSaisi;

		validerNumerique(&nombreSaisi);
		Y[i] = nombreSaisi;
	}
	
	cout << endl;
	cout << "----------------------------------------------------------------" << endl;
	cout << "|          X         |          Y         |          Z         |" << endl;
	cout << "----------------------------------------------------------------" << endl;
	for (int i = 0; i < TAILLE_TABLEAU; i++)
	{
		Z[i] = X[i] * Y[i];
		sommeZ += Z[i];
		cout << "|";
		afficherNombre(X[i]);
		afficherNombre(Y[i]);
		afficherNombre(Z[i]);
		cout << endl;
		cout << "----------------------------------------------------------------" << endl;
	}
	cout  << endl;
	cout << "Racine carree de la somme des elements de Z : " << sqrt(sommeZ) << endl;
	system("pause");

	return 0;
}

void validerNumerique(int* nombreSaisi)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> *nombreSaisi;
	}
}

/*Fonctions pour gerer l'affichage des nombres dans une table de maniere esthetique.
	----------------------------------
	|    X     |    Y     |    Z     |
	----------------------------------
	|         1|         3|         3|
	|         2|         4|         8|
	----------------------------------
*/
void calculTailleNombre(int nombre, int &taille)
{
	int quotient = nombre;
	taille = 1;

	while (quotient > 10)
	{
		quotient /= 10;
		taille++;
	}
}

void afficherNombre(int nombre)
{
	int taille;
	
	const int NOMBRE_CASES = 20;

	calculTailleNombre(nombre, taille);

	for (int i = 0; i < NOMBRE_CASES - taille; i++)
	{
		cout << " ";
	}
	cout << nombre << "|";
}