/*
	Analyse
	Entree
		nombre entier saisi

	Sortie
		Afficher la numeration du nombre

	Donnees internes
		SEUIL = 10000

	Traitement
		Diviser le nombre saisi ainsi que les quotients obtenus, jusqu'a ce qu'on trouve 0 comme quotient.
		Chaque reste correspond a une position dans la numeration, allant du chiffre des unites au chiffre des dizaines de mille.
*/

#include<iostream>

using namespace std;

int main() {
	int valeur, quotient, reste, position = 1;
	int SEUIL = 10000;

	do
	{
		cout << "Saisir une valeur entiere. Ce nombre doit etre positif et inferieure ou egale a " << SEUIL << " : ";
		cin >> valeur;
	} while (valeur <= 0 || valeur > SEUIL);

	quotient = valeur;
	do
	{
		reste = quotient % 10;
		quotient /= 10;
		if (position == 1)
			cout << reste << " unites" << endl;
		else if (position == 2)
			cout << reste << " dizaines" << endl;
		else if (position == 3)
			cout << reste << " centaines" << endl;
		else if (position == 4)
			cout << reste << " unites de mille" << endl;
		else if (position == 5)
			cout << reste << " dizaines de mille" << endl;
		position++;
	} while (quotient > 0);

	system("pause");

	return 0;
}