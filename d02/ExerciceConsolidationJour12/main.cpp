﻿/*
	1) Une fonction qui dessine un sapin dont la hauteur est passée en paramètres.

	Analyse
	Entree
		hauteurSapin

	Sortie
		Sapin

	Donnees internes
		--

	Traitement
		Dessiner un sapin ayant la hauteurSapinSapin donnee
		Pour la ligne 0 (sommet du sapin) nous avons une etoile a la colonne hauteurSapin - 1. Pour la ligne 1, nous avons des etoiles allant de (hauteurSapin - 1) - 1 a (hauteurSapin - 1) + 1.
		Pour la ligne hauteurSapin - 1 (base du sapin), nous avons des etoiles allant de 0 a 2 * hauteurSapin - 1 - 1 = hauteurSapin - 1 + hauteurSapin - 1 = ligne + hauteurSapin - 1. 
		Pour la ligne hateur - 2, nous avons des etoiles allant de 1 a 2 * hauteurSapin - 1 - 2 = hauteurSapin - 2 + hauteurSapin - 1 = ligne + hauteurSapin -1.
		Nous deduisons que pour une ligne donnee, nous avons des etoiles allant de hauteurSapin - 1 - ligne a hauteurSapin - 1 + ligne.

	Prototype
		dessinerSapin(hauteurSapin: entier)
*/

void dessinerSapin(int hauteurSapin);

/*
	2) Une fonction qui calcule an (a puissance n) pour a réel et n entier positif (n≥0).

	Analyse
	Entree
		base de la puissance (a)
		exposant (n)

	Sortie
		a puissance n

	Donnees internes
		--

	Traitement
		Multiplier la base par lui meme un nombre de fois egal a n

	Prototype
	calculPuissance(a: reel, n: entier): reel

*/

double puissanceIntPositif(double a, int n);
double puissanceInt(double a, int n);

/*
	3) Une fonction qui retourne l'arrondi d'un réel x à 10-n près.
		Par exemple : arrondir(3.14159, 4) vaut 3.1416 et arrondir(-1.83, 1) vaut -1.8 .

	Analyse
	Entree
		reel a arrondir
		n

	Sortie
		reel arrondi

	Donnees internes
		--

	Traitement
		Arrondir le reel a la precision donnee

	Prototype
		arrondir(a: reel, n: entier): reel
*/

double arrondir(double x, int n);

/*
	4) Une fonction qui prend un entier n et retourne true si n est un nombre premier, false sinon.

	Analyse
	Entree
		nombre entier

	Sortie
		True ou false

	Donnees internes
		--

	Traitement
		Si le nombre est premier retourner true, sinon retourner false

	Prototype
		nombreEstPremier(nombreEntier: entier): booleen
*/

bool premier(int n);

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	cout << arrondir(3.14159, 4) << endl;
	system("pause");
	return 0;
}

void dessinerSapin(int hauteurSapin)
{
	for (int i = 0; i < hauteurSapin; i++)
	{
		for (int j = 0; j < hauteurSapin - 1 - i; j++)
		{
			cout << " ";
		}
		for (int j = hauteurSapin - 1 - i; j < i + hauteurSapin; j++)
		{
			cout << "*";
		}
		cout << endl;
	}
}

double puissanceIntPositif(double a, int n)
{
	double puissance = 1;

	for (int i = 0; i < n; i++)
	{
		puissance *= a;
	}

	return puissance;
}

double puissanceInt(double a, int n)
{
	double puissance = 1;

	if (n > 0)
	{
		puissance = puissanceIntPositif(a, n);
	}
	else
	{
		puissance = 1 / puissanceIntPositif(a, -1 * n);
	}
	
	return puissance;
}

double arrondir(double x, int n)
{
	int entier;
	int unite;
	double arrondi;

	entier = x * puissanceInt(10, n + 1);
	unite = entier % 10;
	entier = entier / 10;

	if (unite > 5)
	{
		entier += 1;
	}
	
	arrondi = entier * puissanceInt(10, -n);

	return arrondi;
}

bool premier(int n)
{
	bool estPremier = true;

	for (int i = 2; i < n; i++)
	{
		if (n % i == 0)
		{
			estPremier = false;
		}
	}

	return estPremier;
}
