/*
	Exercice 2
	�crire un algorithme qui lit un nombre entier positif entre 0 et 100  et d�termine si c'est un nombre premier. (indice : essayer de diviser ce nombre par les nombres venant avant lui). Valider l�entr�e.

	Analyse
	Entree
		Le nombre saisi par l'utilisateur (nombreSaisi)

	Sortie
		Afficher si le nombre est premier

	Donnees internes
		borne inferieure: 0 (MIN)
		borne superieure: 100 (MAX)

	Traitement
		Faire la division euclidienne de nombreSaisi par tous les nombres venant avant lui, jusqu'a ce qu'il y a au moins une division dont le reste est 0. Dans ce cas donc afficher nombreSaisi n'est pas premier. Sinon afficher nombreSaisi est premier.

*/

#include<iostream>

using namespace std;

int main() {
	int nombreSaisi, nombreAvant, reste;
	const int MIN = 0, MAX = 100;

	cout << "Saisir un nombre entre " << MIN << " et " << MAX << endl;
	cin >> nombreSaisi;
	while (cin.fail() || cin.peek() != '\n' || !(nombreSaisi >= MIN && nombreSaisi <= MAX)) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre superieur ou egal a " << MIN << " et inferieur ou egal a " << MAX << ": ";
		cin >> nombreSaisi;
	}

	nombreAvant = 2;
	do
	{
		reste = nombreSaisi % nombreAvant;
		nombreAvant = nombreAvant + 1;
	} while (reste != 0 && nombreAvant < nombreSaisi);

	if (reste != 0) {
		cout << nombreSaisi << " est premier." << endl;
	}
	else
	{
		cout << nombreSaisi << " n'est pas premier." << endl;
	}

	system("pause");

	return 0;
}