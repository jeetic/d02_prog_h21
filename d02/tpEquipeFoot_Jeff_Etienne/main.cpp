#include <iostream>
using namespace std;

const unsigned short TAILLE_NOM = 31;
const unsigned short TAILLE_PRENOM = 31;
const unsigned short NOMBRE_JOUEURS_MAX = 14;
const char POSITION_JOUEUR[4][18] = { "Gardien de but", "Attaquant", "D�fenseur", "Milieu de terrain" };

enum Position {
	GARDIEN_DE_BUT = 1,
	ATTAQUANT,
	DEFENSEUR,
	MILIEU_DE_TERRAIN
};

enum Option
{
	AJOUTER = 1,
	MODIFIER,
	RETIRER,
	AFFICHER_INFOS,
	AJOUTER_BUT,
	RETIRER_BUT,
	AFFICHER_JOUEURS,
	BUTS_EQUIPES,
	QUITTER
};

unsigned short MENU_PRINCIPAL = 6;
unsigned short TOUS_TYPES_CONFONDUS = 5;
unsigned short BORNE_MIN = 1;
short JOUEUR_INEXISTANT = -1;

struct joueur {
	unsigned short noDeJoueur;
	char nom[TAILLE_NOM];
	char prenom[TAILLE_PRENOM];
	unsigned short nbButs;
	Position position;
};

unsigned short afficherMenu();
short sousMenu();
void ajouterJoueur(joueur joueurs[], unsigned short &nbJoueurs);
int rechercherJoueur(joueur joueurs[], unsigned short nbJoueurs);
void modifierJoueur(joueur &joueur);
void retirerJoueur(joueur joueurs[], unsigned short &nbJoueurs, int indiceJoueurARetirer);
void afficherInfoJoueur(const joueur &JOUEUR);
void ajouterButJoueur(joueur &joueur);
void retirerButJoueur(joueur &joueur);
void afficherJoueursParPosition(joueur joueurs[], unsigned short nbJoueurs);
void afficherTotalDeButDeLEquipe(joueur joueurs[], unsigned short nbJoueurs);

//Fonctions de validations
void validerNumerique(unsigned short* nombreSaisi);
void validerBornes(unsigned short* nombreSaisi, int MIN, int MAX);
void validerChaine(char chaine[], const int TAILLE_CHAINE);

//Fonctions supplementaires
void saisirInfoJoueur(char nom[], char prenom[], Position &position);
void afficherChaine(char chaine[], const int TAILLE_TABLEAU);
void calculerTailleChaine(char chaine[], int &taille);
void calculTailleNombre(int nombre, int &taille);
void afficherNombre(int nombre, const int NOMBRE_CASES);

int main() {
	const locale LOCALE = locale::global(locale(""));
	
	unsigned short nbJoueurs = 0;
	joueur joueurs[NOMBRE_JOUEURS_MAX] = {};
	unsigned short choix, choixType;
	int indiceJoueur;

	do
	{
		system("cls");
		choix = afficherMenu();
		system("cls");
		switch (choix)
		{
		case AJOUTER:
			ajouterJoueur(joueurs, nbJoueurs);
			break;

		case MODIFIER:
			indiceJoueur = rechercherJoueur(joueurs, nbJoueurs);
			if (indiceJoueur >= 0)
			{
				modifierJoueur(joueurs[indiceJoueur]);
			}
			system("pause");
			break;

		case RETIRER:
			indiceJoueur = rechercherJoueur(joueurs, nbJoueurs);
			if (indiceJoueur >= 0)
			{
				retirerJoueur(joueurs, nbJoueurs, indiceJoueur);
			}
			system("pause");
			break;

		case AFFICHER_INFOS:
			indiceJoueur = rechercherJoueur(joueurs, nbJoueurs);
			if (indiceJoueur >= 0)
			{
				afficherInfoJoueur(joueurs[indiceJoueur]);
			}
			system("pause");
			break;

		case AJOUTER_BUT:
			indiceJoueur = rechercherJoueur(joueurs, nbJoueurs);
			if (indiceJoueur >= 0)
			{
				ajouterButJoueur(joueurs[indiceJoueur]);
			}
			system("pause");
			break;

		case RETIRER_BUT:
			indiceJoueur = rechercherJoueur(joueurs, nbJoueurs);
			if (indiceJoueur >= 0)
			{
				retirerButJoueur(joueurs[indiceJoueur]);
			}
			system("pause");
			break;

		case AFFICHER_JOUEURS:
			afficherJoueursParPosition(joueurs, nbJoueurs);
			break;

		case BUTS_EQUIPES:
			afficherTotalDeButDeLEquipe(joueurs, nbJoueurs);
			system("pause");
			break;

		case QUITTER:
			cout << "Voulez-vous vraiment quitter?" << endl;
			cout << "1) Oui" << endl;
			cout << "2) Non" << endl;
			cin >> choixType;
			validerNumerique(&choixType);
			validerBornes(&choixType, 1, 2);

			if (choixType == 2)
			{
				choix = 0;
			}
		}
	} while (choix != QUITTER);


	system("pause");
	return 0;
}

/*
	T�che : fonction qui fait apparaitre un menu, demande de faire un
			choix � l'utilisateur, r�cup�re ce choix et le retourne.
	Param�tre(s) : aucun
	Retour : Le choix effectu� par l'utilisateur
*/
unsigned short afficherMenu() {
	unsigned short choix, choixType;
	cout << "Veuillez choisir l'une des options suivantes" << endl << endl;
	cout << "1 � Ajouter un joueur." << endl;
	cout << "2 � Modifier un joueur." << endl;
	cout << "3 � Retirer un joueur." << endl;
	cout << "4 � Afficher les informations d�un joueur selon son num�ro de joueur." << endl;
	cout << "5 � Ajouter un but � un joueur selon son num�ro de joueur." << endl;
	cout << "6 � Retirer un but � un joueur selon son num�ro de joueur." << endl;
	cout << "7 � Afficher tous les joueurs." << endl;
	cout << "8 � Afficher le nombre total de buts compt�s pour l��quipe." << endl;
	cout << "9 � Quitter l'application" << endl;

	cin >> choix;

	validerNumerique(&choix);
	validerBornes(&choix, AJOUTER, QUITTER);

	return choix;
}

/*
	T�che : fonction qui fait apparaitre un menu lorsque 
			l'utilisateur choisit de faire un affichage dans le menu principal, demande de faire un
			choix � l'utilisateur, r�cup�re ce choix et le retourne.
	Param�tre(s) : aucun
	Retour : Le choix effectu� par l'utilisateur
*/
short sousMenu()
{
	unsigned short choixType;
	system("cls");
	cout << "Veuillez choisir l'une des options suivantes" << endl << endl;
	cout << "1 - Gardien de but" << endl;
	cout << "2 - Attaquant" << endl;
	cout << "3 - Defenseur" << endl;
	cout << "4 - Milieu de terrain" << endl;
	cout << "5 - Tous les types confondus" << endl;
	cout << "6 - Menu principal" << endl;

	cin >> choixType;

	validerNumerique(&choixType);
	validerBornes(&choixType, GARDIEN_DE_BUT, MENU_PRINCIPAL);


	if (choixType == MENU_PRINCIPAL)
	{
		system("cls");
		afficherMenu();
	}

	return choixType;
}

/*
	T�che : fonction qui sert � ins�rer UN SEUL nouveau joueur dans l'�quipe. Demander une saisie pour chacune des informations d�un joueur, sauf pour le nombre de but, qui sera initialis� � 0 au moment de la cr�ation.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
	Retour : aucun (sortie via les param�tres formels pass�s par adresse et/ou r�f�rence)
*/
void ajouterJoueur(joueur joueurs[], unsigned short &nbJoueurs) {
	joueur joueur;
	unsigned short position;
	bool numeroAssigne = false;
	
	cout << "Ajout d'un nouveau joueur" << endl << endl;
	cout << "Saisir le numero du joueur   : ";
	cin >> joueur.noDeJoueur;
	validerNumerique(&joueur.noDeJoueur);
	validerBornes(&joueur.noDeJoueur, BORNE_MIN, NOMBRE_JOUEURS_MAX);

	do
	{
		numeroAssigne = false;
		for (int i = 0; i < nbJoueurs; i++)
		{
			if (joueurs[i].noDeJoueur == joueur.noDeJoueur)
			{
				numeroAssigne = true;
			}
		}
		if (numeroAssigne)
		{
			cout << "***Attention!!! Ce numero appartient a un autre joueur. Saisir un autre numero :";
			cin >> joueur.noDeJoueur;
			validerNumerique(&joueur.noDeJoueur);
			validerBornes(&joueur.noDeJoueur, BORNE_MIN, NOMBRE_JOUEURS_MAX);
		}
	} while (numeroAssigne);

	saisirInfoJoueur(joueur.nom, joueur.prenom, joueur.position);

	joueur.nbButs = 0;

	joueurs[nbJoueurs] = joueur;
	nbJoueurs++;

	cout << endl;
	cout << "Le joueur a �t� ajout� avec succ�s!!!" << endl;
	system("pause");
}

/*
	T�che : fonction qui demande la saisie d'un num�ro de joueur et
			cherche dans la liste des joueurs de l'�quipe dans le
			but de retrouver ce joueur. Si le num�ro du joueur saisi
			correspond au num�ro d'un des joueurs de l'�quipe, la
			fonction retourne l'indice ou se trouve le joueur dans
			le tableau, sinon la fonction affiche le message
			"Ce joueur n'existe pas" et retourne la valeur -1.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
*/
int rechercherJoueur(joueur joueurs[], unsigned short nbJoueurs)
{
	unsigned short numeroJoueur;
	int indiceJoueur = JOUEUR_INEXISTANT;
	cout << "Saisir le numero du joueur : ";
	cin >> numeroJoueur;
	validerNumerique(&numeroJoueur);
	validerBornes(&numeroJoueur, BORNE_MIN, NOMBRE_JOUEURS_MAX);

	for (int i = 0; i < nbJoueurs; i++)
	{
		if (joueurs[i].noDeJoueur == numeroJoueur)
		{
			indiceJoueur = i;
		}
	}

	if (indiceJoueur == JOUEUR_INEXISTANT)
	{
		cout << "Ce joueur n'existe pas" << endl;
	}

	return indiceJoueur;
}

/*
	T�che : fonction qui permet de modifier les informations D'UN SEUL joueur
		 pass� en param�tre. Les seules informations que vous pouvez modifier
		 pour un joueur sont le num�ro du joueur, le nom, le pr�nom et la
		 position. Vous ne devez pas permettre de modifier le nombre de buts.
	Param�tre : une r�f�rence d'un joueur.
*/
void modifierJoueur(joueur &joueur)
{
	cout << "Saisir le nouveau num�ro du joueur : ";
	cin >> joueur.noDeJoueur;
	validerNumerique(&joueur.noDeJoueur);
	validerBornes(&joueur.noDeJoueur, BORNE_MIN, NOMBRE_JOUEURS_MAX);

	saisirInfoJoueur(joueur.nom, joueur.prenom, joueur.position);
}

/*
	T�che : fonction qui retire un joueur du tableau en fonction
			de l'indice o� se trouve le joueur dans le tableau.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
				l'indice o� se trouve joueur dans le tableau
	Retour : aucun (sortie via les param�tres formels pass�s par
			 adresse et/ou r�f�rence)
*/
void retirerJoueur(joueur joueurs[], unsigned short &nbJoueurs, int indiceJoueurARetirer) {
	for (int i = indiceJoueurARetirer; i < nbJoueurs; i++)
	{
		joueurs[i] = joueurs[i + 1];
	}

	nbJoueurs--;
	cout << "Le joueur a �t� retir�!!!" << endl;
}

/*
	T�che : fonction qui affiche les informations D'UN SEUL joueur
			pass� en param�tre.
	Param�tre : une r�f�rence constante d'un joueur pour �viter tout
				risque de modification du joueur dans la fonction.
	Retour : aucun (affichage seulement)
*/
void afficherInfoJoueur(const joueur &JOUEUR) {
	cout << "numero du joueur  : " << JOUEUR.noDeJoueur << endl;
	cout << "nom du joueur     : " << JOUEUR.nom << endl;
	cout << "Prenom du joueur  : " << JOUEUR.prenom << endl;
	cout << "Position du joueur: " << POSITION_JOUEUR[JOUEUR.position - 1] << endl;
	cout << "Nombre de buts    : " << JOUEUR.nbButs << endl;
}

/*
	T�che : fonction qui ajoute un but � un joueur pass� par r�f�rence
			en param�tre.
	Param�tre : une r�f�rence d'un joueur
	Retour : aucun (sortie via les param�tres formels pass�s par
			 adresse et/ou r�f�rence)
*/
void ajouterButJoueur(joueur &joueur) {
	joueur.nbButs++;
	cout << "1 but a �t� ajout� au nombre de buts du joueur " << joueur.prenom << " " << joueur.nom << endl;
}

/*
	T�che : fonction qui retire un but � un joueur pass� par r�f�rence
			en param�tre. La fonction doit pr�voir que le nombre de but
			ne pourra pas jamais �tre inf�rieur � 0.
	Param�tre : une r�f�rence d'un joueur
	Retour : aucun (sortie via les param�tres formels pass�s par
			 adresse et/ou r�f�rence)
*/
void retirerButJoueur(joueur &joueur) {
	if (joueur.nbButs > 0)
	{
		joueur.nbButs--;
		cout << "1 but a �t� retir� du nombre de buts du joueur " << joueur.prenom << " " << joueur.nom << endl;
	}
	else
	{
		cout << "Attention!!! Ce joueur n'a pas de but." << endl;
	}
}

/*
	T�che : fonction qui affiche les joueurs de l'�quipe apr�s avoir
			demand� la saisie de la position � afficher. Aussi, ajouter
			une option permettant d'afficher tous les joueurs.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
	Retour : aucun (affichage seulement)
*/
void afficherJoueursParPosition(joueur joueurs[], unsigned short nbJoueurs) {
	unsigned short position;
	char positionJoueur[18] = {};
	char pasDeJoueur[] = "                                                Pas de joueur";

	position = sousMenu();
	validerNumerique(&position);
	validerBornes(&position, 1, 5);

	system("cls");
	cout << "--------------------------------------------------------------------------------------------------------------" << endl;
	cout << "|                           Les joueurs de l'�quipe correspondant a vos crit�res                             |" << endl;
	cout << "--------------------------------------------------------------------------------------------------------------" << endl;
	cout << "| Numero |             Nom              |            Pr�nom            |      Position      | Nombre de buts |" << endl;
	cout << "--------------------------------------------------------------------------------------------------------------" << endl;
	if (position == 5)
	{
		if (nbJoueurs == 0)
		{
			cout << "|";
			afficherChaine(pasDeJoueur, 108);
			cout << endl;
			cout << "--------------------------------------------------------------------------------------------------------------" << endl;
		}
		for (int i = 0; i < nbJoueurs; i++)
		{
			for (int j = 0; j < 18; j++)
			{
				positionJoueur[j] = POSITION_JOUEUR[joueurs[i].position - 1][j];
			}
			
			cout << "|";
			afficherNombre(joueurs[i].noDeJoueur, 8);
			afficherChaine(joueurs[i].nom, 30);
			afficherChaine(joueurs[i].prenom, 30);
			afficherChaine(positionJoueur, 20);
			afficherNombre(joueurs[i].nbButs, 16);
			cout << endl;
			cout << "--------------------------------------------------------------------------------------------------------------" << endl;
		}
	}
	else
	{
		int nbJoueursCritere = 0;
		for (int i = 0; i < nbJoueurs; i++)
		{
			if (joueurs[i].position == (Position)position)
			{
				
				for (int j = 0; j < 18; j++)
				{
					positionJoueur[j] = POSITION_JOUEUR[joueurs[i].position - 1][j];
				}
				cout << "|";
				afficherNombre(joueurs[i].noDeJoueur, 8);
				afficherChaine(joueurs[i].nom, 30);
				afficherChaine(joueurs[i].prenom, 30);
				afficherChaine(positionJoueur, 20);
				afficherNombre(joueurs[i].nbButs, 16);
				cout << endl;
				cout << "--------------------------------------------------------------------------------------------------------------" << endl;
				nbJoueursCritere++;
			}
		}
		if (nbJoueursCritere == 0)
		{
			cout << "|";
			afficherChaine(pasDeJoueur, 108);
			cout << endl;
			cout << "--------------------------------------------------------------------------------------------------------------" << endl;
		}
	}

	system("pause");
}

/*
	T�che : fonction qui calcule et affiche le nombre total de but(s)
			marqu�(s) pour toutes l'�quipes.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
	Retour : aucun (affichage seulement)
*/
void afficherTotalDeButDeLEquipe(joueur joueurs[], unsigned short nbJoueurs) {
	int totalButsEquipe = 0;
	for (int i = 0; i < nbJoueurs; i++)
	{
		totalButsEquipe += joueurs[i].nbButs;
	}
	cout << "Le total de buts de l'�quipe est " << totalButsEquipe << endl;
}

/*
	T�che : fonction qui valide si la valeur saisie par l'utilisateur est num�rique.
	Param�tre : La valeur saisie par l'utilisateur
	Retour : valeur pass�e par adresse a la fonction
*/
void validerNumerique(unsigned short* nombreSaisi)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> *nombreSaisi;
	}
}

/*
	T�che : fonction qui valide si la valeur saisie par l'utilisateur 
			est comprise entre un minimum et un maximum donn�s.
	Param�tre : La valeur saisie par l'utilisateur (prealablement validee numerique)
	Retour : valeur pass�e par adresse a la fonction
*/
void validerBornes(unsigned short* nombreSaisi, int MIN, int MAX)
{
	while (*nombreSaisi < MIN || *nombreSaisi > MAX) {
		cout << "Attention - Veuillez saisir un nombre compris entre " << MIN << " et " << MAX << " : ";
		cin >> *nombreSaisi;
	}
}

/*
	T�che : fonction qui valide si la chaine saisie par l'utilisateur
			repecte le nombre de caracteres limite requis.
	Param�tre : La chaine saisie par l'utilisateur
				Nombre de caracteres limite requis
	Retour : chaine pass�e par adresse a la fonction
*/
void validerChaine(char chaine[], const int TAILLE_CHAINE)
{
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention : max " << TAILLE_CHAINE << " caract�res : ";
		cin.getline(chaine, TAILLE_CHAINE);
	}
}

/*
	T�che : fonction qui demande la saisie des informations d'un joueur.
	Param�tre : nom, prenom et position
	Retour : les informations passees par reference
*/
void saisirInfoJoueur(char nom[], char prenom[], Position &position)
{
	unsigned short pos;
	cin.ignore(512, '\n');

	cout << "Saisir le nom du joueur      : ";
	cin.getline(nom, TAILLE_NOM);
	validerChaine(nom, TAILLE_NOM);

	cout << "Saisir le pr�nom du joueur   : ";
	cin.getline(prenom, TAILLE_PRENOM);
	validerChaine(prenom, TAILLE_PRENOM);

	cout << "Saisir la position du joueur : ";
	cin >> pos;
	validerNumerique(&pos);
	validerBornes(&pos, GARDIEN_DE_BUT, MILIEU_DE_TERRAIN);
	position = (Position)pos;
}

/*
	T�che : fonction qui calcule la taille d'une chaine de caracteres.
	Param�tre : La chaine et la taille
	Retour : la taille de la chaine passee par reference
*/
void calculerTailleChaine(char chaine[], int &taille)
{
	int i = 0;
	taille = 0;
	while (chaine[i] != '\0')
	{
		taille++;
		i++;
	}
}

/*
	T�che : fonction qui affiche une chaine de caracteres.
	Param�tre : la chaine, le nombre de places disponibles pour l'affichage
	Retour : aucun (Gere seulement l'affichage)
*/
void afficherChaine(char chaine[], const int TAILLE_TABLEAU)
{
	int taille;

	const int NOMBRE_CASES = 20;

	calculerTailleChaine(chaine, taille);

	cout << " " << chaine;
	for (int i = 0; i < TAILLE_TABLEAU - taille - 1; i++)
	{
		cout << " ";
	}
	cout << "|";
}

/*
	T�che : fonction qui calcule le nombre de chiffres dans un nombre.
	Param�tre : Le nombre et la taille
	Retour : la taille passee par reference
*/
void calculTailleNombre(int nombre, int &taille)
{
	int quotient = nombre;
	taille = 1;

	while (quotient >= 10)
	{
		quotient /= 10;
		taille++;
	}
}

/*
	T�che : fonction qui affiche un nombre.
	Param�tre : le nombre, le nombre de places disponibles pour l'affichage
	Retour : aucun (Gere seulement l'affichage)
*/
void afficherNombre(int nombre, const int NOMBRE_CASES)
{
	int taille;

	calculTailleNombre(nombre, taille);

	cout << " " << nombre;
	for (int i = 0; i < NOMBRE_CASES - taille - 1; i++)
	{
		cout << " ";
	}
	cout << "|";
}
