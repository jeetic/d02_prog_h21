#include <iostream>

using namespace std;

void echanger(int* a, int* b);

int main()
{
	int val1 = 10, val2 = 20;

	cout << val1 << " " << val2 << endl;
	echanger(&val1, &val2);
	cout << val1 << " " << val2 << endl;

	system("pause");

	return 0;
}

void echanger(int* a, int* b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

/*
	Le bout de code fera afficher:
	10 20
	20 10
	Car les parametres sont passes par adresse a la fonction echanger. Ils sont donc modifies apres l'appel de la fonction.
*/