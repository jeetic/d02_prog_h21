/*
	Analyse
	Entree:
		nombre entier n
		nombre entier m
	
	Sortie:
		Afficher m prochaines valeurs a partir de n
	
	Donnees internes:
		--
	
	Traitement:
		Afficher les m nombres a partir de n
*/

#include <iostream>

using namespace std;

int main()
{
	int n, m;

	cout << "Saisir la premiere valeur:";
	cin >> n;

	do
	{
		cout << "Saisir la deuxieme valeur. Ce nombre doit etre positif:";
		cin >> m;
	} while (m <= 0);

	for (int i = n; i <= n + m - 1; i++)
	{
		cout << i << endl;
	}

	system("pause");

	return 0;
}