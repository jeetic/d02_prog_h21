#include <iostream>

using namespace std;

/*
	Analyse
	Entree
		tableau d'entiers

	Sortie
		moyenneValeursPaires

	Donnees internes
		--

	Traitement
		Parcourir le tableau, faire la somme des valeur paires et diviser cette somme par le nombre de valeurs paires.
*/

int moyenneValeursPairs(int tab[], const int TAILLE);

int main()
{
	

	system("pause");

	return 0;
}

int moyenneValeursPairs(int tab[], const int TAILLE)
{
	int somme = 0;
	int nbValeursPaires = 0;

	for (int i = 0; i < TAILLE; i++)
	{
		if (tab[i] % 2 == 0)
		{
			nbValeursPaires++;
			somme += tab[i];
		}
	}

	return somme / nbValeursPaires;
}