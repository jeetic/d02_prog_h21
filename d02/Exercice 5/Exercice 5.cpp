/*
	Analyse
	Entree
		Nombre entier
	
	Sortie
		Afficher la valeur inversee de la factorielle
	
	Donnees internes
		--
	
	Traitement
		Faire le produit des diviseurs du nombre entier saisi, tant que ce produit est inferieur au nombre saisi.
*/

#include<iostream>

using namespace std;

int main() {
	int valeur, produit, factorielle;
	bool isFactoriel = true;

	do
	{
		cout << "Saisir une valeur entiere. Ce nombre doit etre positif: ";
		cin >> valeur;
	} while (valeur <= 0);


	produit = 1;
	for (int i = 2; i <= valeur; i++)
	{
		if (valeur % i == 0 && produit < valeur)
		{
			produit *= i;
			factorielle = i;
		}
		if (valeur % i != 0 && produit < valeur)
			isFactoriel = false;
	}
	if (!isFactoriel)
		cout << "Il n'y a pas de factorielle pour le nombre " << valeur << endl;
	else
	{
		cout << "La factorielle est " << factorielle << endl;
		cout << "Validation : " << endl;
		produit = 1;
		for (int i = 2; i <= factorielle; i++)
		{
			cout << produit << " * " << i << " = " << produit * i << endl;
			produit *= i;
		}
	}

	system("pause");

	return 0;
}