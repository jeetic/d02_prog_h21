#include <iostream>

using namespace std;

/*
	Analyse
	Entree
		salaireBrut

	Sortie
		impotAPayer

	Donnees internes
		SEUIL1_IMPOT = 20000
		SEUIL2_IMPOT = 45000
		IMPOT1 = 0.20
		IMPOT2 = 0.25
		IMPOT3 = 0.30

	Traitement
		demander a l'utilisateur de saisir salaireBrut.
		Si salaireBrut < SEUIL1_IMPOT Alors impotAPayer = salaireBrut * IMPOT1
		Sinon Si salaireBrut >= SEUIL1_IMPOT et salaireBrut < SEUIL2_IMPOT Alors impotAPayer = salaireBrut * IMPOT2
		Sinon Si salaireBrut > SEUIL2_IMPOT Alors impotAPayer = salaireBrut * IMPOT3
*/
double montantImpotAPayer(int salaireBrut);

int main()
{
	

	system("pause");

	return 0;
}

double montantImpotAPayer(int salaireBrut)
{
	const double SEUIL1_IMPOT = 20000;
	const double SEUIL2_IMPOT = 45000;
	const double IMPOT1 = 0.20;
	const double IMPOT2 = 0.25;
	const double IMPOT3 = 0.30;

	double impotAPayer;
	if (salaireBrut < SEUIL1_IMPOT)
	{
		impotAPayer = salaireBrut * IMPOT1;
	}
	else if (salaireBrut >= SEUIL1_IMPOT && salaireBrut < SEUIL2_IMPOT)
	{
		impotAPayer = salaireBrut * IMPOT2;
	}
	else
	{
		impotAPayer = salaireBrut * IMPOT3;
	}

	return impotAPayer;
}