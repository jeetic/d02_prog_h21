/*
	Exercice 3
	Analyse
	Entree:
		nombre entier saisie
	
	Sortie:
		Tous les diviseurs du nombre entier saisi
	
	Donnees internes:
		--
	
	Traitement:
		Si le reste de la division euclidienne du nombre saisi par tout nombre inferieur est 0, afficher ce nombre.
*/

#include<iostream>

using namespace std;

int main()
{
	int valeur;

	do
	{
		cout << "Saisir une valeur entiere. Ce nombre doit etre different de 0: ";
		cin >> valeur;
	} while (valeur == 0);

	if (valeur > 0)
	{
		for (int i = 1; i <= valeur; i++)
		{
			if (valeur % i == 0)
			{
				cout << i << endl;
				cout << -i << endl;//Les diviseurs negatifs
			}
		}
	}
	else
	{
		for (int i = -1; i >= valeur; i--)
		{
			if (valeur % i == 0)
			{
				cout << i << endl;
				cout << -i << endl;//Les diviseurs positifs
			}
		}
	}

	system("pause");

	return 0;
}