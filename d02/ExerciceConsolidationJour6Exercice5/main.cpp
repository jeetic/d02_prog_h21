/*
	Analyse
	Entree
		soldeActuel
		montantRetrait

	Sortie
		montantRestant

	Donnees internes
		--

	Traitement
		Si montantRetrait <= soldeActuel soutraire montantRetrait de soldeActuel
*/

#include<iostream>

using namespace std;

int main()
{
	double soldeActuel, montantRetrait, montantRestant;

	cout << "Saisir le montant du solde actuel : ";
	cin >> soldeActuel;

	while (cin.fail() || cin.peek() != '\n' || soldeActuel <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> soldeActuel;
	}

	cout << "Saisir le montant du retrait : ";
	cin >> montantRetrait;

	while (cin.fail() || cin.peek() != '\n' || montantRetrait <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantRetrait;
	}

	montantRestant = soldeActuel;
	if (soldeActuel >= montantRetrait) 
	{
		montantRestant = soldeActuel - montantRetrait;
	}
	else
	{
		cout << "Le solde de votre compte n'est pas suffisant pour faire ce retrait." << endl;
	}

	cout << "Le montant restant est " << montantRestant << "$" << endl;

	system("pause");

	return 0;
}