/*
	Exercice1
	Analyse
	Entree
		Deux entiers a et b

	Sortie
		Somme de a et b

	Donnees internes
		--

	Traitement 
		Retourner a + b

	Prototype
		somme(a: entier, b: entier): entier

	Exercice2
	Analyse
	Entree:
		Longuer
		Largeur
		hauteur

	Sortie:
		Volume du parallelepipede

	Donnees internes:
		--

	Traitement:
		Multiplier la longueur, la largeur et la hauteur, et retourner le produit

	Exercice3
	Analyse
	Entree:
		Deux entiers a et b

	Sortie:
		Le plus grand des deux entiers

	Donnees internes:
		--

	Traitement:
		Comparer a et b et retourner le plus grand
*/

#include <iostream>

using namespace std;

int somme(int a, int b);

double volume(double longueur, double largeur, double hauteur);

int max(int a, int b);

int main()
{
	
	return 0;
}

//Exercice 1
int somme(int a, int b)
{
	return a + b;
}

//Exercice 2
double volume(double longueur, double largeur, double hauteur)
{
	return longueur * largeur * hauteur;
}

int max(int a, int b)
{
	int max;
	if (a > b)
	{
		max = a;
	}
	else
	{
		max = b;
	}
	return max;
}