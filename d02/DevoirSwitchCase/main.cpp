#include<iostream>

using namespace std;

int main() {
	char animal;
	short couleur;
	cout << "Quel animal? (C)hien ou Ch(a)t : ";
	cin >> animal;
	cout << "Choisir une couleur (0 = noir, 1 = brun, 2 = blanc): ";
	cin >> couleur;

	if (animal == 'C' || animal == 'c')
	{
		cout << "Vous avez choisi un chien ";
	}
	else if(animal == 'A' || animal == 'a')
	{
		cout << "Vous avez choisi un chat";
	}

	switch (couleur)
	{
	case 2: 
		cout << "blanc" << endl;
		break;
	case 1: 
		cout << "brun" << endl;
		break;
	default:
		cout << "noir" << endl;
		break;
	}
	return 0;
}