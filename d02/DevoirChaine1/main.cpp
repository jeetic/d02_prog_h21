#include <iostream>

using namespace std;

int main()
{
	char nom[31], prenom[31];
	const int TAILLE = 31;
	cout << "Saisir le nom : ";
	cin.getline(nom, TAILLE);

	cout << "Saisir le prenom : ";
	cin.getline(prenom, TAILLE);

	cout << "Bonjour " << prenom << " " << nom << endl;

	system("pause");
	return 0;
}