/*
	Analyse
	Entree:
		Option saisie par l'utilisateur
	
	Sortie:
		Afficher menu
	
	Donnees internes:
		SAISIE1 = 'A'
		SAISIE2 = 'S'
		SAISIE3 = 'M'
		SAISIE4 = 'Q'
	
	Traitement:
		Afficher le menu. Puis demander a l'utilisateur de choisir une option tant qu'il n'a pas choisi SAISIE4.
*/

#include <iostream>

using namespace std;

int main()
{
	char valeur;
	char SAISIE1 = 'A', SAISIE2 = 'S', SAISIE3 = 'M', SAISIE4 = 'Q';

	cout << "Choisir l'une des options suivantes" << endl;
	cout << "(A)jouter 1" << endl;
	cout << "(S)oustraire 4" << endl;
	cout << "(M)ultiplier par 2" << endl;
	cout << "(Q)uitter" << endl << endl;

	do
	{
		cout << "Votre saisie: ";
		cin >> valeur;
		if (valeur != SAISIE1 && valeur != SAISIE2 && valeur != SAISIE3 && valeur != SAISIE4)
			cout << "Valeur saisie incorrecte. Veuillez choisir l'une des quatres options!!!" << endl << endl;
		else
		{
			cout << "Vous avez choisi: " << valeur << endl << endl;
			cout << "Veuillez choisir une autre option" << endl;
			cout << "(A)jouter 1" << endl;
			cout << "(S)oustraire 4" << endl;
			cout << "(M)ultiplier par 2" << endl;
			cout << "(Q)uitter" << endl << endl;
		}
	} while (valeur != SAISIE4);

	return 0;
}