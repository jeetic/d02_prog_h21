/*
	Analyse
	Entree
		typeDepense
		montantDepense

	Sortie
		totalDepensesParCategorie
		totalDepenses

	Donnees internes
		Categorie
		{
			ASSURANCE,
			DIVERS,
			HABILLEMENT,
			LOISIR,
			LOYER,
			NOURRITURE,
			TRANSPORT
		}
		SAISIE_DE_SORTIE = -1

	Traitement
		Demander a l'utilisateur de saisir typeDepense et montantDepense, 
		et additionner les montants de depense pour chaque type, 
		tant que l'utilisateur ne saisit pas SAISIE_DE_SORTIE. 
		Finalement, additionner toutes les depenses.

*/

#include <iostream>

using namespace std;

enum Categorie
{
	ASSURANCE,
	DIVERS,
	HABILLEMENT,
	LOISIR,
	LOYER,
	NOURRITURE,
	TRANSPORT
};

double validerNumerique(double nombreSaisi);
void validerBornes(int* nombreSaisi, int MIN, int MAX);

int main()
{
	const short TAILLE_CATEGORIE = 7;

	int type = 0;
	Categorie typeDepense;
	double montantParType;
	double montantDepense[TAILLE_CATEGORIE] = { 0 };
	double totalDepenses = 0;

	const short SAISIE_DE_SORTIE = -1;
	const char* TYPE_DEPENSE[TAILLE_CATEGORIE] = { "Assurance", "Divers", "Habillement", "Loisir", "Loyer", "Nourriture", "Transport" };

	cout << "Saisir le type de la depense" << endl;
	cout << "0 pour Assurance" << endl;
	cout << "1 pour Divers" << endl;
	cout << "2 pour Habillement" << endl;
	cout << "3 pour Loisir" << endl;
	cout << "4 pour Loyer" << endl;
	cout << "5 pour Nourriture" << endl;
	cout << "6 pour Transport" << endl;

	cin >> type;

	type = validerNumerique(type);
	validerBornes(&type, SAISIE_DE_SORTIE, TRANSPORT);

	while (type != SAISIE_DE_SORTIE)
	{
		

		typeDepense = (Categorie)type;

		cout << "Saisir le montant de la depense pour la categorie " << TYPE_DEPENSE[typeDepense] << " : ";
		cin >> montantParType;

		montantParType = validerNumerique(montantParType);

		montantDepense[typeDepense] += montantParType;

		cout << "Saisir le type de la depense" << endl;
		cout << "0 pour Assurance" << endl;
		cout << "1 pour Divers" << endl;
		cout << "2 pour Habillement" << endl;
		cout << "3 pour Loisir" << endl;
		cout << "4 pour Loyer" << endl;
		cout << "5 pour Nourriture" << endl;
		cout << "6 pour Transport" << endl;

		cin >> type;

		type = validerNumerique(type);
		validerBornes(&type, SAISIE_DE_SORTIE, TRANSPORT);
	}

	for (int i = 0; i < TAILLE_CATEGORIE; i++)
	{
		cout << "Le montant total des depenses pour " << TYPE_DEPENSE[i] << " est " << montantDepense[i] << "$" << endl;
		totalDepenses += montantDepense[i];
	}

	cout << "Le montant total de toutes les depenses est " << totalDepenses << "$" << endl;

	system("pause");

	return 0;
}

double validerNumerique(double nombreSaisi)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> nombreSaisi;
	}
	return nombreSaisi;
}

void validerBornes(int* nombreSaisi, int MIN, int MAX)
{
	while (*nombreSaisi < MIN || *nombreSaisi > MAX) {
		cout << "Attention - Veuillez saisir un nombre compris entre " << MIN << " et " << MAX << " : ";
		cin >> *nombreSaisi;
	}
}

