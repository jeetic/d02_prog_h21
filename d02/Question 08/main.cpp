#include <iostream>

using namespace std;

struct article {

	char nom[101];

	unsigned int quantite;

	double prix;

};

double calculerSousTotal(double prix, unsigned int quantite);
double calculerTaxeQuebec(double montant, const double TPS, const double TVQ);
void afficherArticle(article &article);

double validerNumerique(double nombreSaisi);
double validerBornes(double nombreSaisi);

int main()
{
	const locale LOCALE = locale::global(locale(""));

	const int TAILLE_CHAINE = 100;
	article article;

	cout << "Saisir le nom de l'article (maximum " << TAILLE_CHAINE << " caract�res) : ";
	cin.getline(article.nom, TAILLE_CHAINE);
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention : max " << TAILLE_CHAINE << " caract�res : ";
		cin.getline(article.nom, TAILLE_CHAINE);
	}
	//cin.ignore(512, '\n');

	cout << "Saisir le prix de l'article : ";
	cin >> article.prix;
	validerNumerique(article.prix);
	validerBornes(article.prix);

	cout << "Saisir la quantit� : ";
	cin >> article.quantite;
	validerNumerique(article.quantite);
	validerBornes(article.quantite);

	cout << endl;
	cout << "----------------------- Affichage ----------------------------" << endl;
	afficherArticle(article);

	system("pause");

	return 0;
}

double calculerSousTotal(double prix, unsigned int quantite)
{
	return prix * quantite;
}

double calculerTaxeQuebec(double montant, const double TPS, const double TVQ)
{
	double montantTaxes;
	montantTaxes = montant * TPS + montant * TVQ;
	return montantTaxes;
}

void afficherArticle(article &article)
{
	double const TPS = 0.05, TVQ = 0.09975;
	double sousTotal;
	double montantTaxes;
	double grandTotal;

	sousTotal = calculerSousTotal(article.prix, article.quantite);
	montantTaxes = calculerTaxeQuebec(sousTotal, TPS, TVQ);
	grandTotal = sousTotal + montantTaxes;

	cout << "Nom de l'article                   : " << article.nom << endl;
	cout << "Quantit�                           : " << article.quantite << endl;
	cout << "Prix                               : " << article.prix << endl;
	
	cout << "Le sous-total pour cet article est : " << sousTotal << "$" << endl;
	cout << "Le montant de la taxe est          : " << montantTaxes << "$" << endl;
	cout << "Le grand total est                 : " << grandTotal << "$" << endl;
}

double validerNumerique(double nombreSaisi)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre valide : ";
		cin >> nombreSaisi;
	}
	return nombreSaisi;
}

double validerBornes(double nombreSaisi)
{
	while (nombreSaisi < 0) {
		cout << "Attention - Veuillez saisir un nombre positif : ";
		cin >> nombreSaisi;
	}

	return nombreSaisi;
}