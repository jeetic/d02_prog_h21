#include <iostream>

using namespace std;

int main()
{
	int val = 5; //adresse de la variable val : 06FF9E52
	int *pval = &val; //adresse de la variable pval: 06FF5C34
	int& rval = val; //adresse de la variable rval : 06FF9E52

	/*1.*/ cout << val << endl;
	/*2.*/ cout << &val << endl;
	/*3.*/ cout << *val << endl;
	/*4.*/ cout << pval << endl;
	/*5.*/ cout << &pval << endl;
	/*6.*/ cout << *pval << endl;
	/*7.*/ cout << rval << endl;
	/*8.*/ cout << *rval << endl;
	/*9.*/ cout << &rval << endl;

	/*
		a) Dans 1, la valeur 5 sera afficher. Car on se contente d'afficher le contenu de la variable val

		b) Dans 2, 06FF9E52 sera affiche. Car on affiche l'adresse de la variable val

		c) Dans 3, il y aura une erreur a la compilation. Car l'operateur * place devant un pointeur sert a afficher la valeur contenue dans la zone memoire referencee par le pointeur
			Or, val n'est pas un pointeur, l'utilisation de * n'est valide

		d) Dans 4, 06FF9E52, qui est l'adresse de val sera affiche. Car pval est un pointeur qui reference la variable val

		e) Dans 5, 06FF5C34 sera affiche. Car on affiche l'adresse de la variable pval.

		f) Dans 6, la valeur 5 sera affichee. Car l'operateur * place devant un pointeur sert a afficher la valeur contenue dans la zone memoire referencee par le pointeur.
			pval pointe sur la zone memoire de val qui contient 5.

		g) Dans 7, la valeur 5 sera affichee. rval fait une reference vers la valeur dans la zone memoire de val.

		h) Dans 8, il y aura une erreur a la compilation. * place devant un pointeur sert a afficher la valeur contenue dans la zone memoire referencee par le pointeur
			Or, rval n'est pas un pointeur, l'utilisation de * n'est valide

		i) Dans 9, 06FF9E52 sera affiche. Car on affiche l'adresse de la variable rval

		j) Les adresses de val et de rVal sont identiques, car rval fait reference a la zone memoire de val.
	*/

	system("pause");

	return 0;
}