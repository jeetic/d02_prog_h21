/*
	Analyse
	Entree
		nombreSaisi

	Sortie
		resultatTentative

	Donnees internes
		NOMBRE_MIN = 1
		NOMBRE_MAX = 100
		NOMBRE_CHANCES = 10
		CHARTES_PRIX_GAGNES = { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 }
		ResultatsPossibles
		{
			TROP_PETIT = -1,
			EGAL,
			TROP_GRAND
		}

	Traitement
		//Joueur joue
		Generer un nombre aleatoire et demande a l'utilisateur de saisir un nombre. 
		Comparer le nombreSaisi et le nombre genere.
		Si le nombreSaisi est inferieur au nombre genere afficher trop petit et ajouter 1 au nombre de tentatives
		Si le nombreSaisi est superieur au nombre genere afficher trop grand et ajouter 1 au nombre de tentatives
		Si le nombreSaisi est egal au nombre genere afficher vous avez gagne et afficher le montant du gain suivant le nombre de tentatives.

		//Ordinateur joue
		Generer un premier nombre aleatoire et demande a l'ordinateur de deviner en generant un autre nombre aleatoire,
		tant que le second nombre genere est different du premier. 
		A chaque tentative changer les bornes MIN et MAX suivant les valeurs devinees par l'ordinateur. 
		Si le nombre devine est trop petit, charger la borne minimale a la valeur devinee + 1. 
		Si le nombre devine est trop grand, changer la borne maximale a la valeur devinee - 1.
	Prototype
		genererValeurAleatoire()
		validerNumerique(nombreSaisi: entier)
		validerBornes(nombreSaisi: entier)
		determinerSiTrouve(nombreSaisi: entier)
		determinerMontantGain(nombreTentative: entier)
		joueurJoue()
		ordinateurJoue()
*/

#include <iostream>
#include <time.h>

using namespace std;

enum ResultatsPossibles
{
	TROP_PETIT = -1,
	EGAL,
	TROP_GRAND
};

double CHARTES_PRIX_GAGNES[10] = { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 };

void genererValeurAleatoire(const int MAX, const int MIN, int* nombreGenere);
void validerNumerique(int* nombreSaisi);
void validerBornes(int* nombreSaisi, int MIN, int MAX);
void determinerSiTrouve(int nombreSaisi, int nombreGenere, int* trouve);
void determinerMontantGain(int nombreTentative, double* montantGain);
void joueurJoue();
void ordinateurJoue();
/*
	Pour simuler un temps de reflexion de l'ordinateur
*/
void wait(int seconds);

int main()
{
	ordinateurJoue();

	system("pause");

	return 0;
}

void genererValeurAleatoire(const int MIN, const int MAX, int* nombreGenere)
{
	srand(time(0));
	int randomGeneratedNumber = rand();
	*nombreGenere = (randomGeneratedNumber % (MAX - MIN + 1)) + MIN;
}

void validerNumerique(int* nombreSaisi)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> *nombreSaisi;
	}
}

void validerBornes(int* nombreSaisi, int MIN, int MAX)
{
	while (*nombreSaisi < MIN || *nombreSaisi > MAX) {
		cout << "Attention - Veuillez saisir un nombre compris entre " << MIN << " et " << MAX << " : ";
		cin >> *nombreSaisi;
	}
}

void determinerSiTrouve(int nombreSaisi, int nombreGenere, int* trouve)
{
	if (nombreGenere == nombreSaisi)
	{
		cout << "Vous l'avez trouve!!!" << endl;
		*trouve = EGAL;
	}
	else if(nombreSaisi < nombreGenere)
	{
		cout << "Trop petit!!!" << endl;
		*trouve = TROP_PETIT;
	}
	else
	{
		cout << "Trop grand!!!" << endl;
		*trouve = TROP_GRAND;
	}
}

void determinerMontantGain(int nombreTentative, double* montantGain)
{
	switch (nombreTentative)
	{
	case 1:
		*montantGain = CHARTES_PRIX_GAGNES[0];
		break;
	case 2:
		*montantGain = CHARTES_PRIX_GAGNES[1];
		break;
	case 3:
		*montantGain = CHARTES_PRIX_GAGNES[2];
		break;
	case 4:
		*montantGain = CHARTES_PRIX_GAGNES[3];
		break;
	case 5:
		*montantGain = CHARTES_PRIX_GAGNES[4];
		break;
	case 6:
		*montantGain = CHARTES_PRIX_GAGNES[5];
		break;
	case 7:
		*montantGain = CHARTES_PRIX_GAGNES[6];
		break;
	case 8:
		*montantGain = CHARTES_PRIX_GAGNES[7];
		break;
	case 9:
		*montantGain = CHARTES_PRIX_GAGNES[8];
		break;
	case 10:
		*montantGain = CHARTES_PRIX_GAGNES[9];
		break;
	default: 
		*montantGain = 0;
		break;
	}
}

void joueurJoue()
{
	const int NOMBRE_MIN = 1;
	const int NOMBRE_MAX = 100;
	const int NOMBRE_CHANCES = 10;

	int nombreGenere;
	int nombreSaisi;
	int trouve;
	double montantGain;
	int nombreTentatives = 0;

	genererValeurAleatoire(NOMBRE_MIN, NOMBRE_MAX, &nombreGenere);

	do
	{
		cout << "Devineer un nombre." << endl;
		cout << "Saisir le nombre devine: " << endl;
		cin >> nombreSaisi;

		validerNumerique(&nombreSaisi);
		validerBornes(&nombreSaisi, NOMBRE_MIN, NOMBRE_MAX);

		determinerSiTrouve(nombreSaisi, nombreGenere, &trouve);
		nombreTentatives++;
	} while (nombreSaisi != nombreGenere && nombreTentatives <= NOMBRE_CHANCES);

	determinerMontantGain(nombreTentatives, &montantGain);

	if (montantGain > 0)
	{
		cout << "Vous avez gagne " << montantGain << "$" << endl;
	}
	else
	{
		cout << "Vous l'aurez la prochaine fois. Ne vous decouragez pas!!!" << endl;
	}	
}

void ordinateurJoue()
{
	const int NOMBRE_MIN = 1;
	const int NOMBRE_MAX = 100;
	const int NOMBRE_CHANCES = 10;

	int nombreGenere;
	int nombreDevine;
	int trouve;
	double montantGain;
	int nombreTentatives = 0;
	int dernierMinDevine = NOMBRE_MIN, dernierMaxDevine = NOMBRE_MAX;

	cout << "Saisir un nombre entre " << NOMBRE_MIN << " et " << NOMBRE_MAX << " inclusivement: ";
	cin >> nombreGenere;
	validerNumerique(&nombreGenere);
	validerBornes(&nombreGenere, NOMBRE_MIN, NOMBRE_MAX);
	//genererValeurAleatoire(NOMBRE_MIN, NOMBRE_MAX, &nombreGenere);

	do
	{
		cout << "Devineer un nombre entre " << dernierMinDevine << " et " << dernierMaxDevine << "." << endl;
		cout << "Saisir le nombre devine: " << endl;
		wait(5);
		genererValeurAleatoire(dernierMinDevine, dernierMaxDevine, &nombreDevine);

		cout << nombreDevine << endl;

		determinerSiTrouve(nombreDevine, nombreGenere, &trouve);
		if (trouve == TROP_GRAND)
		{
			dernierMaxDevine = nombreDevine - 1;
		}
		else
		{
			dernierMinDevine = nombreDevine + 1;
		}
		nombreTentatives++;
	} while (nombreDevine != nombreGenere && nombreTentatives <= NOMBRE_CHANCES);

	determinerMontantGain(nombreTentatives, &montantGain);

	if (montantGain > 0)
	{
		cout << "Vous avez gagne " << montantGain << "$" << endl;
	}
	else
	{
		cout << "Vous l'aurez la prochaine fois. Ne vous decouragez pas!!!" << endl;
	}
}

void wait(int seconds)
{
	clock_t endwait;
	endwait = clock() + seconds * CLOCKS_PER_SEC;
	while (clock() < endwait) {}
}