/*
	Exercice 7
	Analyse
	Entree
		nombre1
		nombre2
		nombre3

	Sortie
		Afficher si l'un des nombres est egal a la somme de deux autres

	Donnees internes
		--

	Traitement
		Verifier si chacun des nombres est egal a la somme des deux autres
*/

#include<iostream>

using namespace std;

int main() {
	double nombre1, nombre2, nombre3;

	cout << "Saisir le premier nombre" << endl;
	cin >> nombre1;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre valide : ";
		cin >> nombre1;
	}

	cout << "Saisir le premier nombre" << endl;
	cin >> nombre2;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre valide : ";
		cin >> nombre2;
	}

	cout << "Saisir le premier nombre" << endl;
	cin >> nombre3;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre valide : ";
		cin >> nombre3;
	}

	if (nombre1 == nombre2 + nombre3) {
		cout << nombre1 << " est la somme de " << nombre2 << " et " << nombre3 << endl;
	}
	else if (nombre2 == nombre1 + nombre3)
	{
		cout << nombre2 << " est la somme de " << nombre1 << " et " << nombre3 << endl;
	}
	else if (nombre3 == nombre1 + nombre2)
	{
		cout << nombre3 << " est la somme de " << nombre1 << " et " << nombre2 << endl;
	}
	else
	{
		cout << "Pas de solution" << endl;
	}

	system("pause");

	return 0;
}