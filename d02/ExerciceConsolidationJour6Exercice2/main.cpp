/*
	Exercice 2
	Analyse
	Entree:
		travailSession
		examenIntra
		examenFinal

	Sortie:
		noteFinale

	Donnee internes:
		POURCENTAGE_TRAVAIL_SESSION = 0.40
		POURCENTAGE_EXAMEN_INTRA = 0.25
		POURCENTAGE_EXAMEN_FINAL = 0.35
		NOTE_MAXIMALE = 100

	Traitement:
		noteFinale = POURCENTAGE_TRAVAIL_SESSION * travailSession + POURCENTAGE_EXAMEN_INTRA * examenIntra + POURCENTAGE_EXAMEN_FINAL * examenFinal
*/

#include <iostream>

using namespace std;

int main()
{
	double travailSession, examenIntra, examenFinal, noteFinale;
	const double POURCENTAGE_TRAVAIL_SESSION = 0.40, POURCENTAGE_EXAMEN_INTRA = 0.25, POURCENTAGE_EXAMEN_FINAL = 0.35, NOTE_MAXIMALE = 100;

	cout << "Saisir la note du travail de session: ";
	cin >> travailSession;

	while (cin.fail() || cin.peek() != '\n' || travailSession < 0 || travailSession > NOTE_MAXIMALE) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive et inferieure ou egale a " << NOTE_MAXIMALE << " : ";
		cin >> travailSession;
	}

	cout << "Saisir la note de l'examen intra: ";
	cin >> examenIntra;

	while (cin.fail() || cin.peek() != '\n' || examenIntra < 0 || examenIntra > NOTE_MAXIMALE) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive et inferieure ou egale a " << NOTE_MAXIMALE << " : ";
		cin >> examenIntra;
	}

	cout << "Saisir la note de l'examen final: ";
	cin >> examenFinal;

	while (cin.fail() || cin.peek() != '\n' || examenFinal < 0 || examenFinal > NOTE_MAXIMALE) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive et inferieure ou egale a " << NOTE_MAXIMALE << " : ";
		cin >> examenFinal;
	}

	noteFinale = POURCENTAGE_TRAVAIL_SESSION * travailSession + POURCENTAGE_EXAMEN_INTRA * examenIntra + POURCENTAGE_EXAMEN_FINAL * examenFinal;

	cout << "La note finale de l'etudiant est " << noteFinale << "/" << NOTE_MAXIMALE << endl;

	system("pause");
	return 0;
}