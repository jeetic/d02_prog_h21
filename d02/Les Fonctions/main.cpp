﻿/*
	1) Une fonction qui valide la saisie d'un entier compris entre 2 bornes et qui retourne cet entier.
	Analyse
	Entree:
		entier saisi

	Sortie:
		L'entier valide

	Donnees internes:
		Les bornes

	Traitement:
		Afficher : Saisir un entier entre les bornes tant que l'entier n'est pas valide. Retourner l'entier.

	Prototype
		validerEntierSaisi(entierSaisi: entier): entier
*/

int validerEntierSaisi(int entierSaisi);

/*
	2) Une fonction qui retourne l'arrondi d'un réel x à 10-n près.
		Par exemple : arrondir(3.14159, 4) vaut 3.1416 et arrondir(-1.83, 1) vaut -1.8 .

	Analyse
	Entree
		reel a arrondir
		precision

	Sortie
		reel arrondi

	Donnees internes
		--

	Traitement
		Arrondir le reel a la precision donnee

	Prototype
		arrondir(reelArrondir: reel, precision: entier): reel		
*/

double arrondir(double reelArrondir, int precision);

/*
	3) Une fonction qui dessine un sapin dont la hauteur est passée en paramètres.

	Analyse
	Entree
		hauteur du sapin

	Sortie
		Sapin

	Donnees internes
		--

	Traitement
		Dessiner un sapin ayant la hauteur donnee

	Prototype
		dessinerSapin(hauteurSapin: reel): dessinSapin
*/

struct dessinSapin
{
	double hauteurSapin;
	//Autres parametres
};

dessinSapin dessinerSapin(double hauteurSapin);

/*
	4) Une fonction qui calcule an (a puissance n) pour a réel et n entier positif (n≥0).

	Analyse
	Entree
		base de la puissance (a)
		exposant (n)

	Sortie
		a puissance n

	Donnees internes
		--

	Traitement
		Multiplier la base par lui meme un nombre de fois egal a exposant

	Prototype
	calculPuissance(base: reel, exposant: entier): reel

*/

long double calculPuissance(double base, int exposant);

/*
	5) Une fonction qui prend un entier n et retourne true si n est un nombre premier, false sinon.

	Analyse
	Entree
		nombre entier

	Sortie
		True ou false

	Donnees internes
		--

	Traitement
		Si le nombre est premier retourner true, sinon retourner false

	Prototype
		nombreEstPremier(nombreEntier: entier): booleen
*/

bool nombreEstPremier(int nombreEntier);