#include <iostream>

using namespace std;

/*
	Analyse
	Entree
		noteReelle
		ponderationInitiale
		ponderationDesiree

	Sortie
		Afficher noteConvertie

	Donnees internes
		--

	Traitement
		noteConvertie = (noteReelle * ponderationDesiree) / ponderationInitiale
		Afficher noteConvertie
*/

void afficherConversionNote(double noteReelle, double ponderationInitiale, double ponderationDesiree);

int main()
{
	

	system("pause");

	return 0;
}

void afficherConversionNote(double noteReelle, double ponderationInitiale, double ponderationDesiree)
{
	const locale LOCALE = locale::global(locale(""));
	double noteConvertie = (noteReelle * ponderationDesiree) / ponderationInitiale;
	cout << "La note " << noteReelle << "/" << ponderationInitiale << " convertie � une pond�ration sur " << ponderationDesiree << " donne le r�sultat suivant : " << noteConvertie << "/" << ponderationDesiree << endl;
}