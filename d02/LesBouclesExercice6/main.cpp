/*
	Exercice 6
	Analyse
	Entree
		poids d'une lettre

	Sortie
		Sous-total
		Reduction
		Taxe
		Grand total de l'envoi

	Donnee internes
		TPS = 0.05
		TVQ = 0.09975
		SEUIL1 = 27
		SEUIL2 = 81
		SEUIL3 = 120
		COUT_SEUIL1 = 0.45
		COUT_SEUIL2 = 0.90
		COUT_SEUIL3 = 1.10
		COUT_SEUIL3_PLUS = 1.50
		SEUIL_RABAIS = 20
		RABAIS = 0.01
		SEUIL_ARRET = 0

	Traitement
		Demander a l'utilisateur de saisir le poids de la lettre et additionner le prix lie au poids a chaque saisie, jusqu'a ce que l'utilisateur saisie SEUIL_ARRET comme poids.
		Si le prix total est au moins SEUIL_RABAIS, calculer la reduction en multipliant RABAIS par le prix total.
		Calculer le montant du TPS et du TVQ, et les additionner pour trouver le montant des taxes.
		Calculer le grand total de l'envoi en additionnant le montant des taxes et en soustrayant la reduction au prix total.
*/

#include<iostream>

using namespace std;

int main() {
	double poidsLettre, sousTotal, reduction, montantTaxes, grandTotalEnvoi, montantTPS, montantTVQ;
	const double TPS = 0.05;
	const double TVQ = 0.09975;
	const double SEUIL1 = 27;
	const double SEUIL2 = 81;
	const double SEUIL3 = 120;
	const double COUT_SEUIL1 = 0.45;
	const double COUT_SEUIL2 = 0.90;
	const double COUT_SEUIL3 = 1.10;
	const double COUT_SEUIL3_PLUS = 1.50;
	const double SEUIL_ARRET = 0;
	const double SEUIL_RABAIS = 20;
	const double RABAIS = 0.01;

	cout << "Saisir le poids de la lettre. Veuillez Saisir un valeur positive." << endl;
	cin >> poidsLettre;
	while (cin.fail() || cin.peek() != '\n' || poidsLettre <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre positif (>0): ";
		cin >> poidsLettre;
	}

	sousTotal = 0;
	do {
		if (poidsLettre > 0)
		{
			if (poidsLettre < SEUIL1) {
				sousTotal = sousTotal + COUT_SEUIL1;
			}
			else if (poidsLettre >= SEUIL1 && poidsLettre < SEUIL2)
			{
				sousTotal = sousTotal + COUT_SEUIL2;
			}
			else if (poidsLettre >= SEUIL2 && poidsLettre < SEUIL3)
			{
				sousTotal = sousTotal + COUT_SEUIL3;
			}
			else
			{
				sousTotal = sousTotal + COUT_SEUIL3_PLUS;
			}


		}
		else
		{
			cout << "Veuillez saisir un nombre positif !!!" << endl;
		}

		cout << "Saisir le poids de la lettre. Saisir 0 pour finir." << endl;
		cin >> poidsLettre;
	} while (poidsLettre != SEUIL_ARRET);

	reduction = 0;
	if (sousTotal > SEUIL_RABAIS)
	{
		reduction = sousTotal * RABAIS;
		montantTPS = (sousTotal - reduction) * TPS;
		montantTVQ = (sousTotal - reduction) * TVQ;
	}
	else
	{
		montantTPS = sousTotal * TPS;
		montantTVQ = sousTotal * TVQ;
	}
	montantTaxes = montantTPS + montantTVQ;
	grandTotalEnvoi = sousTotal + montantTaxes - reduction;

	cout << "Le sous-total est " << sousTotal << endl;
	cout << "La reduction est " << reduction << endl;
	cout << "Le montant des taxes est " << montantTaxes << endl;
	cout << "Le grand total est " << grandTotalEnvoi << endl;

	system("pause");

	return 0;
}