/*
	Analyse
	Entree:
		Borne inferieure
		Borne superieure
		Nombre saisi
	
	Sortie:
		Afficher si le nombre saisi est compris entre les bornes inferieure et superieure
	
	Donnees internes
		--
	
	Traitement
		Comparer le nombre saisi aux bornes inferieure et superieure, et afficher s'il est compris entre les deux bornes.
*/

#include<iostream>

using namespace std;

int main()
{
	double min, max, valeur;

	cout << "Saisir la borne inferieure: ";
	cin >> min;

	do
	{
		cout << "Saisir la borne superieure. Cette valeur doit etre superieure a " << min << ": ";
		cin >> max;
	} while (max <= min);

	do
	{
		cout << "Saisir une valeur: ";
		cin >> valeur;
		if(valeur <= min || valeur >= max)
			cout << "Attention: la valeur saisie doit etre plus grandes que " << min << " et plus petite que " << max << endl;
	} while (valeur <= min || valeur >= max);

	cout << "La valeur saisie " << valeur << " est plus grande que " << min << " et plus petite que " << max << endl;

	system("pause");

	return 0;
}