/*
Exercice 4
Analyse
Entree
	--

Sortie
	Suite de Fibonacci

Donnees internes
	LIMITE_SEQUENCE = 100

Traitement
	Afficher 0 et 1. POur le reste, afficher la somme des deux nombre qui precedent.
*/

#include<iostream>

using namespace std;

int main() {
	int I, premierQuiPrecede, deuxiemeQuiPrecede, sommeDeuxPrecedent;
	const int LIMITE_SEQUENCE = 50;

	premierQuiPrecede = 1;
	deuxiemeQuiPrecede = 0;

	cout << deuxiemeQuiPrecede << endl;
	cout << premierQuiPrecede << endl;

	for (int I = 2; I <= LIMITE_SEQUENCE; I++) {
		sommeDeuxPrecedent = premierQuiPrecede + deuxiemeQuiPrecede;
		cout << sommeDeuxPrecedent << endl;
		deuxiemeQuiPrecede = premierQuiPrecede;
		premierQuiPrecede = sommeDeuxPrecedent;
	}

	system("pause");

	return 0;
}