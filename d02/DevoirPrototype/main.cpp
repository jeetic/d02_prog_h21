using namespace std;

//1) fonction qui prend 2 entiers a et b, et qui retourne leur somme.
/*
	Analyse
	Entree:
		Deux entiers a et b

	Sortie:
		Somme de a et b

	Donnees internes:
		--

	Traitement:
		Additionner a et b et retourner leur somme
*/
int sommeDeuxEntiers(int a, int b);

//2) Une fonction qui calcule le volume d'un parall�l�pip�de (ou prisme � base rectangulaire).
/*
	Analyse
	Entree:
		Longuer
		Largeur
		hauteur

	Sortie:
		Volume du parallelepipede

	Donnees internes:
		--

	Traitement:
		Multiplier la longueur, la largeur et la hauteur, et retourner le produit
*/
double volumeParallelepipede(double longueur, double largeur, double hauteur);

//3) Une fonction qui prend 2 entiers a et b, et qui retourne le plus grand des 2 entiers.
/*
	Analyse
	Entree:
		Deux entiers a et b

	Sortie:
		Le plus grand des deux entiers

	Donnees internes:
		--

	Traitement:
		Comparer a et b et retourner le plus grand
*/
int plusGrandDeDeuxEntiers(int a, int b);

//4) Une fonction qui retourne la somme des entiers compris entre 2 entiers min et max donn�s.
/*
	Analyse
	Entree:
		Deux entiers min et max

	Sortie:
		Somme des entiers entre min et max

	Donnees internes:
		--

	Traitement:
		Additionner tous les nombres entiers, allant de min + 1 a max-1 et retourner la somme
*/
int sommeEntiersEntreMinEtMax(int min, int max);

//5) Une fonction qui valide la saisie d'un entier et qui retourne cet entier.
/*
	Analyse
	Entree:
		Entier saisi

	Sortie:
		L'entier saisi apres qu'il ait ete valide

	Donnees internes:
		--

	Traitement:
		Valider l'entier saisi puis le retourner s'il est valide
*/
int validationEntierSaisi(int entierSaisi);

