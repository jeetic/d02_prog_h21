#include <iostream>

using namespace std;

bool division(int dividende, int diviseur);

int main()
{

	system("pause");
	return 0;
}

bool division(int dividende, int diviseur)
{
	bool divisionPossible = true;

	if (diviseur == 0)
	{
		cout << "Division par 0 impossible." << endl;
		divisionPossible = false;
	}
	else
	{
		cout << "La division de " << dividende << " / " << diviseur << " = " << (double)dividende/diviseur << "." << endl;
		divisionPossible = true;
	}
	return divisionPossible;
}