/*
	Analyse
	Entree
		ageClient
		jour

	Sortie
		prixBillet
		montantRabais
		ageClient
		jour de la semaine

	Donnees internes
		AGE_MIN = 16
		AGE_MAX = 65
		PRIX_BASE_BILLET = 9
		RABAIS_MINEUR_VIEUX = {0.01, 0.03, 0.03, 0.03, 0.01, 0.01, 0.01}
		RABAIS_MAJEUR = {0.02, 0, 0, 0.02, 0, 0, 0} 

	Traitement
		si ageClient < AGE_MIN et > AGE_MAX alors
		montantRabais = PRIX_BASE_BILLET * RABAIS_MINEUR_VIEUX[jour - 1]
		sinon
		montantRabais = PRIX_BASE_BILLET * RABAIS_MAJEUR[jour - 1]

		prixBillet = PRIX_BASE_BILLET - montantRabais
*/

#include <iostream>

using namespace std;

enum Jour
{
	LUNDI = 1,
	MARDI,
	MERCREDI,
	JEUDI,
	VENDREDI,
	SAMEDI,
	DIMANCHE
};

int validerSaisie(int valeurSaisie);

int main()
{
	short ageClient, jourSemaine;
	double montantRabais, prixBillet;
	Jour jour;
	const short AGE_MIN = 16, AGE_MAX = 65;
	const double PRIX_BASE_BILLET = 9;
	const double RABAIS_MINEUR_VIEUX[7] = { 0.1, 0.3, 0.3, 0.3, 0.1, 0.1, 0.1 };
	const double RABAIS_MAJEUR[7] = { 0.2, 0, 0, 0.2 };

	cout << "Saisir l'age du client : ";
	cin >> ageClient;
	ageClient = validerSaisie(ageClient);

	cout << "Saisir le jour de la semaine : " << endl;
	cout << "1 pour Lundi" << endl;
	cout << "2 pour Mardi" << endl;
	cout << "3 pour Mercredi" << endl;
	cout << "4 pour Jeudi" << endl;
	cout << "5 pour Vendredi" << endl;
	cout << "6 pour Samedi" << endl;
	cout << "7 pour Dimanche" << endl;
	cin >> jourSemaine;
	jourSemaine = validerSaisie(jourSemaine);
	jour = (Jour)jourSemaine;

	cout << "L'age du client est " << ageClient << endl;
	cout << "Le jour de la semaine est ";

	switch (jour)
	{
	case LUNDI:
		cout << "lundi" << endl;
		break;
	case MARDI:
		cout << "mardi" << endl;
		break;
	case MERCREDI:
		cout << "mercredi" << endl;
		break;
	case JEUDI:
		cout << "jeudi" << endl;
		break;
	case VENDREDI:
		cout << "vendredi" << endl;
		break;
	case SAMEDI:
		cout << "samedi" << endl;
		break;
	case DIMANCHE:
		cout << "dimanche" << endl;
		break;
	}
	

	montantRabais = (ageClient < AGE_MIN || ageClient > AGE_MAX ? PRIX_BASE_BILLET * RABAIS_MINEUR_VIEUX[jour - 1] : PRIX_BASE_BILLET * RABAIS_MAJEUR[jour - 1]);
	
	prixBillet = PRIX_BASE_BILLET - montantRabais;
	
	cout << "Le montant du rabais est " << montantRabais << "$" << endl;
	cout << "Le prix du billet est " << prixBillet << "$" << endl;

	system("pause");
	return 0;
}

int validerSaisie(int valeurSaisie)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> valeurSaisie;
	}
	return valeurSaisie;
}