/*
	Analyse
	Entree:
		montantDepart
		dureeDepotTerme
		interetAnnuel

	Sortie
		Montant sur le compte apres la duree du depot a terme ans

	Donnees internes:
		--

	Traitement
		repeter l'addition de l'interet annuel sur le montant total pour chaque annee, jusqu'a DUREE_DEPOT_TERME
*/

#include<iostream>

using namespace std;

int main() {
	double montantTotalApresDuree, montantInteretAnnuel, montantDepart, dureeDepotTerme, interetAnnuel;
	int I;

	cout << "Saisir le montant de depart. Veuillez saisir un nombre positif (>0)" << endl;
	cin >> montantDepart;
	while (cin.fail() || cin.peek() != '\n' || montantDepart <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre positif (>0): ";
		cin >> montantDepart;
	}

	cout << "Saisir la duree du depot a terme. Veuillez saisir un nombre positif (>0)" << endl;
	cin >> dureeDepotTerme;
	while (cin.fail() || cin.peek() != '\n' || dureeDepotTerme <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre positif (>0): ";
		cin >> dureeDepotTerme;
	}

	cout << "Saisir l'interet annuel. Veuillez saisir un nombre positif (>0)" << endl;
	cin >> interetAnnuel;
	while (cin.fail() || cin.peek() != '\n' || interetAnnuel <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre positif (>0): ";
		cin >> interetAnnuel;
	}

	montantTotalApresDuree = montantDepart;
	for (int I = 1; I <= dureeDepotTerme; I++) {
		montantInteretAnnuel = montantTotalApresDuree * interetAnnuel;
		montantTotalApresDuree = montantTotalApresDuree + montantInteretAnnuel;
	}
	cout << "Apres " << dureeDepotTerme << " ans, l'epargne vaudra " << montantTotalApresDuree << "$" << endl;

	system("pause");

	return 0;
}