#include <iostream>

using namespace std;

int main()
{
	char adresse[51];
	const int TAILLE = 51;
	
	cin.ignore(512, '\n');
	cout << "Saisir l'adresse : ";
	cin.getline(adresse, TAILLE);

	while (cin.fail())
	{
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention : max " << TAILLE << " caracteres : ";
		cin.getline(adresse, TAILLE);
	}

	cout << "L'adresse saisie est " << adresse << endl;

	system("pause");
	return 0;
}