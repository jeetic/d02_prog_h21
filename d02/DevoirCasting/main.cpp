#include<iostream>

using namespace std;

int main() {
	int resultat1 = 20 / 3;
	cout << resultat1 << endl;//La valeur de resultat1 est 6. Une division entiere a ete effectuee car les deux valeurs sont entieres.
	double resultat2 = 20 / 3; 
	cout << resultat2 << endl;//La valeur de resultat2 contiendra 6 pour les memes raisons
	double resultat3 = (double)20 / 3; 
	cout << resultat3 << endl;//La valeur de resultat3 est maintenant 6.66667 car on a force la valeur 20 d'etre un reel. Le quotient d'un reel par un entier est un reel.
	double resultat4 = (double)(20 / 3); 
	cout << resultat4 << endl;//La valeur de resultat4 est 6 car on a fait la division des deux entiers d'abord, ce qui a donne 6. Puis on a force la valeur a un reel, ce qui donne toujours 6

	system("pause");
	return 0;
}