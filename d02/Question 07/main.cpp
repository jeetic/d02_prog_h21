#include <iostream>

using namespace std;

enum Mois
{
	JANVIER,
	FEVRIER,
	MARS,
	AVRIL,
	MAI,
	JUIN,
	JUILLET,
	AOUT,
	SEPTEMBRE,
	OCTOBRE,
	NOVEMBRE,
	DECEMBRE
};

const char MOIS_EN_LETTRES[12][10] = { "Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "D�cembre" };

void afficherMois(Mois mois);

int main()
{
	

	system("pause");

	return 0;
}

void afficherMois(Mois mois)
{
	const locale LOCALE = locale::global(locale(""));
	cout << "Le mois en toute lettre est " << MOIS_EN_LETTRES[mois] << endl;
}