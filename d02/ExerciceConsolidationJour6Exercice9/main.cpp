/*
	Exercice 9
	Analyse
	Entree
		valeur1
		valeur2
		valeur3

	Sortie
		Afficher la plus grande valeur

	Donnees internes
		--

	Traitement
		Comparer les nombres et afficher le plus grand.
*/

#include<iostream>

using namespace std;

int main()
{
	double valeur1, valeur2, valeur3, plusGrandeValeur;

	cout << "Saisir la premiere valeur : ";
	cin >> valeur1;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre valide : ";
		cin >> valeur1;
	}

	cout << "Saisir la deuxieme valeur : ";
	cin >> valeur1;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre valide : ";
		cin >> valeur2;
	}

	cout << "Saisir la troisieme valeur : ";
	cin >> valeur3;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - Saisir un nombre valide : ";
		cin >> valeur3;
	}

	plusGrandeValeur = valeur1;
	if (valeur1 > valeur2 && valeur1 > valeur3)
	{
		cout << valeur1 << " est la plus grande valeur" << endl;
	}
	else if(valeur2 > valeur1 && valeur2 > valeur3)
	{
		cout << valeur2 << " est la plus grande valeur" << endl;
	}
	else
	{
		cout << valeur3 << " est la plus grande valeur" << endl;
	}
	
	system("pause");

	return 0;
}