#include <iostream>

using namespace std;

/*
	Analyse
	Entree
		tableau d'entiers

	Sortie
		nbEntiersNegatifs

	Donnees internes
		--

	Traitement
		Parcourir le tableau et augmenter nbEntiersNegatifs de 1 pour chaque nombre negatif rencontré.
*/

int compterNombreNegatifDansTableau(int tableauEntiers[], const int TAILLE);

int main()
{
	
	system("pause");

	return 0;
}

int compterNombreNegatifDansTableau(int tableauEntiers[], const int TAILLE)
{
	int nbEntiersNegatifs = 0;

	for (int i = 0; i < TAILLE; i++)
	{
		if (tableauEntiers[i] < 0)
		{
			nbEntiersNegatifs++;
		}
	}

	return nbEntiersNegatifs;
}
