Football Partie 1
Analyse
Entree:
	age du joueur
	
Sortie:
	Moyenne d'age par equipe et pour la ligue
	
Donnees internes:
	AGE_MIN = 18
	AGE_MAX = 65
	NOMBRE_EQUIPES = 9
	NOMBRE_JOUEURS_PAR_EQUIPE = 20

Traitement:
	pour chaque equipe, demande a l'utilisateur de saisir l'age de chaque joueur. 
	Calculer la somme des ages, et la diviser par NOMBRE_JOUEURS_PAR_EQUIPE (moyenne d'age par equipe) 
	et diviser le resultat par NOMBRE_EQUIPES (moyenne d'age pour la ligue).