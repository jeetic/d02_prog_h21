#include <iostream>

using namespace std;

int main()
{
	int ageJoueur, sommeAgesEquipe = 0, moyenneAgeEquipe, sommeMoyenneEquipes = 0;
	const int NOMBRE_EQUIPES = 9, NOMBRE_JOUEURS_PAR_EQUIPE = 20, AGE_MIN = 18, AGE_MAX = 65;

	for (int i = 0; i < NOMBRE_EQUIPES; i++)
	{
		for (int j = 0; j < NOMBRE_JOUEURS_PAR_EQUIPE; j++)
		{
			do
			{
				cout << "Saisir l'age du joueur. Ce nombre doit etre entre " << AGE_MIN << " et " << AGE_MAX << ": ";
				cin >> ageJoueur;
			} while (!(ageJoueur >= AGE_MIN && ageJoueur <= AGE_MAX));

			sommeAgesEquipe += ageJoueur;
		}

		moyenneAgeEquipe = sommeAgesEquipe / NOMBRE_JOUEURS_PAR_EQUIPE;
		sommeMoyenneEquipes += moyenneAgeEquipe;
		cout << "La moyenne d'ages pour l'equipe " << i << " est " << moyenneAgeEquipe << endl << endl;
		sommeAgesEquipe = 0;
	}

	cout << "La moyenne d'ages pour la ligue est " << sommeMoyenneEquipes / NOMBRE_EQUIPES << endl;

	//system("pause");

	return 0;
}