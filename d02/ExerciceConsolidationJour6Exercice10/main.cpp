/*
	Exercice 10
	Analyse
	Entree
		ancienneValeur
		nombreAccidents

	Sortie
		nouvelle valeur de la prime

	Donnees internes
		SEUIL_ACCIDENT_MIN = 0
		SEUIL_ACCIDENT2 = 2
		SEUIL_ACCIDENT3 = 3
		SEUIL_ACCIDENT4 = 4
		AUGMENTATION_MIN = 0.02
		AUGMENTATION2 = 0.05
		AUGMENTATION3 = 0.10
		AUGMENTATION4 = 0.30

	Traitement
		Calcul de l'augmentation
		si nombreAccidents = SEUIL_ACCIDENT_MIN alors augmentation = ancienneValeur * AUGMENTATION_MIN
		si nombreAccidents <= SEUIL_ACCIDENT2 et nombreAccidents > SEUIL_ACCIDENT_MIN alors augmentation = ancienneValeur * AUGMENTATION2
		si nombreAccidents = SEUIL_ACCIDENT3 alors augmentation = ancienneValeur * AUGMENTATION3
		si nombreAccidents >= SEUIL_ACCIDENT4 alors augmentation = ancienneValeur * AUGMENTATION4
*/

#include<iostream>

using namespace std;

int main()
{
	double ancienneValeur, nouvelleValeur, augmentation;
	int nombreAccidents;


	const int SEUIL_ACCIDENT_MIN = 0, SEUIL_ACCIDENT2 = 2, SEUIL_ACCIDENT3 = 3, SEUIL_ACCIDENT4 = 4;
	const double AUGMENTATION_MIN = 0.02, AUGMENTATION2 = 0.05, AUGMENTATION3 = 0.10, AUGMENTATION4 = 0.30;

	cout << "Saisir l'ancienne valeur de la prime. Veuillez saisir un nombre positif : ";
	cin >> ancienneValeur;
	while (cin.fail() || cin.peek() != '\n' || ancienneValeur <= 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> ancienneValeur;
	}

	cout << "Saisir le nombre d'accident. Veuillez saisir un nombre superieur ou egal a zero : ";
	cin >> nombreAccidents;
	while (cin.fail() || cin.peek() != '\n' || nombreAccidents < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> nombreAccidents;
	}

	if (nombreAccidents = 0)
	{
		augmentation = ancienneValeur * AUGMENTATION_MIN;
	}
	else if(nombreAccidents > SEUIL_ACCIDENT_MIN && nombreAccidents <= SEUIL_ACCIDENT2)
	{
		augmentation = ancienneValeur * AUGMENTATION2;
	}
	else if (nombreAccidents = SEUIL_ACCIDENT3)
	{
		augmentation = ancienneValeur * AUGMENTATION3;
	}
	else if (nombreAccidents >= SEUIL_ACCIDENT4)
	{
		augmentation = ancienneValeur * AUGMENTATION4;
	}
	
	nouvelleValeur = nouvelleValeur + augmentation;

	cout << "La nouvelle valeur de la prime est " << nouvelleValeur << "$" << endl;

	system("pause");

	return 0;
}