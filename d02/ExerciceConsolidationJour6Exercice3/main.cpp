/*
	Exercice 2
	Analyse
	Entree:		
		depensesNourritureMenagers
		depensesCourantes
		
		montantCarteTransport
		montantLoyer
		montantFacturesMensuelles
		
		montantChequePaye1
		montantChequePaye2

	Sortie:
		depensestotales
		gainTotal

	Donnee internes:
		--

	Traitement:
		depensesHebdomadaires = depensesNourritureMenagers + depensesCourantes
		depensesMensuelles = montantCarteTransport + montantLoyer + montantFacturesMensuelles
		depensesTotales = depensesHebdomadaires * 4 + depensesMensuelles

		gainTotal = montantChequePaye1 + montantChequePaye2

		difference = gainTotal - depensesTotal
*/

#include <iostream>

using namespace std;

int main()
{
	double depensesNourritureMenagers, depensesCourantes, montantCarteTransport, montantLoyer, montantFacturesMensuelles, montantChequePaye1, montantChequePaye2;
	double depensesHebdomadaires, depensesMensuelles, depensestotales, gainTotal;

	cout << "Saisir le montant des depenses pour nourriture et produits menagers : ";
	cin >> depensesNourritureMenagers;

	while (cin.fail() || cin.peek() != '\n' || depensesNourritureMenagers < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> depensesNourritureMenagers;
	}

	cout << "Saisir le montant des depenses courantes : ";
	cin >> depensesCourantes;

	while (cin.fail() || cin.peek() != '\n' || depensesCourantes < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> depensesCourantes;
	}

	cout << "Saisir le montant de la carte de transport en commun : ";
	cin >> montantCarteTransport;

	while (cin.fail() || cin.peek() != '\n' || montantCarteTransport < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantCarteTransport;
	}

	cout << "Saisir le montant du loyer : ";
	cin >> montantLoyer;

	while (cin.fail() || cin.peek() != '\n' || montantLoyer < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantLoyer;
	}

	cout << "Saisir le montant des factures mensuelles : ";
	cin >> montantFacturesMensuelles;

	while (cin.fail() || cin.peek() != '\n' || montantFacturesMensuelles < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantFacturesMensuelles;
	}

	cout << "Saisir le montant du premier cheque : ";
	cin >> montantChequePaye1;

	while (cin.fail() || cin.peek() != '\n' || montantChequePaye1 < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantChequePaye1;
	}

	cout << "Saisir le montant du deuxieme cheque : ";
	cin >> montantChequePaye2;

	while (cin.fail() || cin.peek() != '\n' || montantChequePaye2 < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantChequePaye2;
	}

	cout << "Saisir le montant du loyer : ";
	cin >> montantLoyer;

	while (cin.fail() || cin.peek() != '\n' || montantLoyer < 0) {
		cin.clear(); //
		cin.ignore(512, '\n');
		cout << "Attention - valeur positive : ";
		cin >> montantLoyer;
	}

	depensesHebdomadaires = depensesNourritureMenagers + depensesCourantes;
	depensesMensuelles = montantCarteTransport + montantLoyer + montantFacturesMensuelles;
	depensestotales = depensesHebdomadaires * 4 + depensesMensuelles;

	gainTotal = montantChequePaye1 + montantChequePaye2;

	cout << "Le total des depenses est " << depensestotales << "$" << endl;
	cout << "Le total des gain est " << gainTotal << "$" << endl;
	cout << "La difference est " << gainTotal - depensestotales << "$" << endl;

	system("pause");
	return 0;
}