/*
	Exercice 3
	a)
	Analyse
	Entree:
		--

	Sortie
		Montant sur le compte apres 5 ans

	Donnees internes:
		MONTANT_DEPART = 500000
		DUREE_DEPOT_TERME = 5
		INTERET_ANNUEL = 0.10

	Traitement
		repeter l'addition de l'interet annuel sur le montant total pour chaque annee, jusqu'a DUREE_DEPOT_TERME
*/

#include<iostream>

using namespace std;

int main() {
	double montantTotalApresDuree, montantInteretAnnuel;
	int I;
	const double MONTANT_DEPART = 500000, DUREE_DEPOT_TERME = 5, INTERET_ANNUEL = 0.10;

	montantTotalApresDuree = MONTANT_DEPART;
	for (int I = 1; I <= DUREE_DEPOT_TERME; I++) {
		montantInteretAnnuel = montantTotalApresDuree * INTERET_ANNUEL;
		montantTotalApresDuree = montantTotalApresDuree + montantInteretAnnuel;
	}
	cout << "Apres " << DUREE_DEPOT_TERME << " ans, l'epargne vaudra " << montantTotalApresDuree << "$" << endl;

	system("pause");

	return 0;
}