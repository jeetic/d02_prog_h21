/*
	Analyse
	Entrees:
		capacite en litre du reservoir
		nombre de places assises
		date de mise en service

	Sorties:
		Afficher les informations saisies

	Donnees internes:
		--

	Traitement:
		Demander a l'utilisateur de saisir les informations, puis les afficher.
	*/

#include<iostream>

using namespace std;

enum Mois {
	JANVIER,
	FEVRIER,
	MARS,
	AVRIL,
	MAI,
	JUIN,
	JUILLET,
	AOUT,
	SEPTEMBRE,
	OCTOBRE,
	NOVEMBRE,
	DECEMBRE
};

//d)
struct date
{
	int jour, mois, annee;
};

struct avion
{
	double capaciteReservoir;
	int nbPlacesAssises;
	date miseEnService;
};

int main()
{
	//b)
	/* Ajouter un / au debut de la ligne pour enlever le commentaire dans b) et commenter d)
	double capaciteReservoir;
	int nbPlacesAssises;
	int jour, mois, annee;

	do
	{
		cout << "Saisir la capacite en litres du reservoir de l'avion. Ce nombre doit etre positif: ";
		cin >> capaciteReservoir;
	} while (capaciteReservoir <= 0);
	
	do
	{
		cout << "Saisir le nombre de places assises. Ce nombre doit etre positif: ";
		cin >> nbPlacesAssises;
	} while (nbPlacesAssises <= 0);
	
	do
	{
		cout << "Saisir le mois. " << Mois::JANVIER << " pour janvier, " << Mois::FEVRIER << " pour fevrier, " << Mois::MARS << " pour mars etc: ";
		cin >> mois;
	} while (mois < 0 || mois > 11);

	do
	{
		cout << "Saisir l'annee: ";
		cin >> annee;
	} while (annee <= 0);

	do
	{
		cout << "Saisir le jour: ";
		cin >> jour;
	} while ((annee % 4 == 0 && mois == Mois::FEVRIER && jour > 29) || (annee % 4 != 0 && mois == Mois::FEVRIER && jour > 28) || jour < 1 || (mois < Mois::JUILLET && mois % 2 == 0 && jour > 31) || (mois >= Mois::JUILLET && mois % 2 == 1 && jour > 31) || (mois < Mois::JUILLET && mois % 2 == 1 && jour > 31) || (mois >= Mois::JUILLET && mois % 2 == 0 && jour > 30));

	//c)
	
	cout << "La capacite du reservoir de l'avion est " << capaciteReservoir << " litres." << endl;
	cout << "Le nombre de place assises est " << nbPlacesAssises << endl;

	cout << "La date de mise en service est ";
	if (mois == Mois::JANVIER)
	{
		cout << jour << " janvier " << annee << endl;
	}
	else if (mois == Mois::FEVRIER)
	{
		cout << jour << " fevrier " << annee << endl;
	}
	else if (mois == Mois::MARS)
	{
		cout << jour << " mars " << annee << endl;
	}
	else if (mois == Mois::AVRIL)
	{
		cout << jour << " avril " << annee << endl;
	}
	else if (mois == Mois::MAI)
	{
		cout << jour << " mai " << annee << endl;
	}
	else if (mois == Mois::JUIN)
	{
		cout << jour << " juin " << annee << endl;
	}
	else if (mois == Mois::JUILLET)
	{
		cout << jour << " juillet " << annee << endl;
	}
	else if (mois == Mois::AOUT)
	{
		cout << jour << " aout " << annee << endl;
	}
	else if (mois == Mois::SEPTEMBRE)
	{
		cout << jour << " septembre " << annee << endl;
	}
	else if (mois == Mois::OCTOBRE)
	{
		cout << jour << " octobre " << annee << endl;
	}
	else if (mois == Mois::NOVEMBRE)
	{
		cout << jour << " novembre " << annee << endl;
	}
	else if (mois == Mois::DECEMBRE)
	{
		cout << jour << " decembre " << annee << endl;
	}
	/*/

	//d)
	avion avion1;
	int nbPlacesAssisesAccumulees = 0;
	char reponse;

	do//e)
	{
		do
		{
			cout << "Saisir la capacite en litres du reservoir de l'avion. Ce nombre doit etre positif: ";
			cin >> avion1.capaciteReservoir;
		} while (avion1.capaciteReservoir <= 0);

		do
		{
			cout << "Saisir le nombre de places assises. Ce nombre doit etre positif: ";
			cin >> avion1.nbPlacesAssises;
		} while (avion1.nbPlacesAssises <= 0);

		do
		{
			cout << "Saisir le mois. " << Mois::JANVIER << " pour janvier, " << Mois::FEVRIER << " pour fevrier, " << Mois::MARS << " pour mars etc: ";
			cin >> avion1.miseEnService.mois;
		} while (avion1.miseEnService.mois < 0 || avion1.miseEnService.mois > 11);

		do
		{
			cout << "Saisir l'annee: ";
			cin >> avion1.miseEnService.annee;
		} while (avion1.miseEnService.annee <= 0);

		do
		{
			cout << "Saisir le jour: ";
			cin >> avion1.miseEnService.jour;
		} while ((avion1.miseEnService.annee % 4 == 0 && avion1.miseEnService.mois == Mois::FEVRIER && avion1.miseEnService.jour > 29)
			|| (avion1.miseEnService.annee % 4 != 0 && avion1.miseEnService.mois == Mois::FEVRIER && avion1.miseEnService.jour > 28)
			|| avion1.miseEnService.jour < 1
			|| (avion1.miseEnService.mois < Mois::JUILLET && avion1.miseEnService.mois % 2 == 0 && avion1.miseEnService.jour > 31)
			|| (avion1.miseEnService.mois >= Mois::JUILLET && avion1.miseEnService.mois % 2 == 1 && avion1.miseEnService.jour > 31)
			|| (avion1.miseEnService.mois < Mois::JUILLET && avion1.miseEnService.mois % 2 == 1 && avion1.miseEnService.jour > 31)
			|| (avion1.miseEnService.mois >= Mois::JUILLET && avion1.miseEnService.mois % 2 == 0 && avion1.miseEnService.jour > 30));

		cout << "La capacite du reservoir de l'avion est " << avion1.capaciteReservoir << " litres." << endl;
		cout << "Le nombre de place assises est " << avion1.nbPlacesAssises << endl;

		cout << "La date de mise en service est ";
		if (avion1.miseEnService.mois == Mois::JANVIER)
		{
			cout << avion1.miseEnService.jour << " janvier " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::FEVRIER)
		{
			cout << avion1.miseEnService.jour << " fevrier " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::MARS)
		{
			cout << avion1.miseEnService.jour << " mars " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::AVRIL)
		{
			cout << avion1.miseEnService.jour << " avril " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::MAI)
		{
			cout << avion1.miseEnService.jour << " mai " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::JUIN)
		{
			cout << avion1.miseEnService.jour << " juin " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::JUILLET)
		{
			cout << avion1.miseEnService.jour << " juillet " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::AOUT)
		{
			cout << avion1.miseEnService.jour << " aout " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::SEPTEMBRE)
		{
			cout << avion1.miseEnService.jour << " septembre " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::OCTOBRE)
		{
			cout << avion1.miseEnService.jour << " octobre " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::NOVEMBRE)
		{
			cout << avion1.miseEnService.jour << " novembre " << avion1.miseEnService.annee << endl;
		}
		else if (avion1.miseEnService.mois == Mois::DECEMBRE)
		{
			cout << avion1.miseEnService.jour << " decembre " << avion1.miseEnService.annee << endl;
		}

		nbPlacesAssisesAccumulees += avion1.nbPlacesAssises;

		cout << "Voulez-vous saisir des informations pour un autre avions? o pour oui et n pour non: ";
		cin >> reponse;
		cout << endl;
	} while (reponse != 'n');

	//f)
	cout << "Le nombre de places assises accumulees est " << nbPlacesAssisesAccumulees << endl;
	//*/


	system("pause");

	return 0;
}