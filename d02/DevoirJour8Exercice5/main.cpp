#include <iostream>

using namespace std;

void echanger(int* a, int* b);

int main()
{
	int val1 = 10, val2 = 20;

	cout << val1 << " " << val2 << endl;
	echanger(&val1, &val2);
	cout << val1 << " " << val2 << endl;

	system("pause");

	return 0;
}

void echanger(int* a, int* b)
{
	int c = a;
	a = b;
	b = c;
}

/*
	Il y aura une erreur a la compilation. Car dans int c = a; et b = c, c est de type int et a et b sont des pointeurs.
	On ne peut pas assigner un pointeur a un entier.
*/